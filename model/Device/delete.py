"""

This file implements the device deletion functionality of the website.
The file consists of the Delete class which contains the following methods:-
    1)  __init__ - Initialises the parameters/variables
    2)  del_device - Implements the functionality to delete a particular inactive device

"""

from config.messages  import device_deletion_messages
from model.basemodel import Basemodel
from bson.objectid import ObjectId


class Delete(Basemodel):

    def __init__(self):

        """

        This method acts as a constructor and initialises the parameters obtained from the form

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None

    def del_device(self, user_id, device_id):

        """

        This method implements the functionality to delete a particular device which is in inactive state

        :param user_id:
        :param device_id:
        :return:

        """

        print ("In del_device block")

        self.user_id = user_id
        self.device_id = ObjectId(device_id)
        if self.db.acedevices.find({"userId" : ObjectId(self.user_id)}).count() >= 1: # User exists in the database
            print ("User exists")
            if self.db.acedevices.find({"_id" : ObjectId(self.device_id)}).count() >= 1: # Device exists in the database:
                print ("Device exists")
                item = self.db.acedevices.find_one({"_id": ObjectId(self.device_id)})
                status = item.get('status')
                if status == "0": # Device is not enabled -> It can be deleted
                    self.db.acedevices.remove({"_id": ObjectId(self.device_id)})
                    message = device_deletion_messages["MSG_001"]
                else: # Device is enabled -> Cannot be deleted
                    message = device_deletion_messages["MSG_003"]
            else: # Device does not exist in the database
                message = device_deletion_messages["MSG_002"]
        else: # User not found in the database
            message = device_deletion_messages["MSG_004"]

        return message




