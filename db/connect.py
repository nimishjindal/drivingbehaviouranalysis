import pymongo
try:
    from urllib import quote  # Python 2.X
except ImportError:
    from urllib.parse import quote  # Python 3+
from config import constants as const


class Database():

    def __init__(self):

        """

        This method acts as a constructor and initialises the parameters/variables

        """

        self.client = None
        #self.dbhost_server = const.database['DBHOST_SERVER']
        self.dbhost_local = const.database['DBHOST_LOCAL']
        self.dbname = const.database['DBNAME']
        self.dbuser = const.database['DBUSER']
        self.dbpass = const.database['DBPASS']
        
        self.dbhost = self.dbhost_local

    def initialise_client(self):

        """
        This method initiates a connection to the database making use of the following :-
            1) Database user
            2) Database password
            3) Database host server
            4) Pymongo

        If the connection is successful, we return the database client.

        """
        try:
            self.dbconn = "mongodb://"+self.dbuser+":" + quote(self.dbpass) + "@"+self.dbhost+"/"+self.dbname
            self.client = pymongo.MongoClient(self.dbconn)
            db = self.client[self.dbname ]
            return db
        except Exception as e:
            print(e)
            return e
