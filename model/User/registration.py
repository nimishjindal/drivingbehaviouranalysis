"""

This file is used for implementing the registration part of the website.
The file consists of the Registration class which has the following methods :-
    1) __init__ - Works as a constructor to initialise the variables/parameters
    2) register_user - Has the code for interacting with the database and implementing the registration section

"""

from config.messages  import registration_messages
from model.basemodel import Basemodel


class Registration(Basemodel):

    def __init__(self, username, email, password, lastLogin, createdon,
                    updatedon, status, firstName, lastName):

        """
        This method initialises the parameters/variables
        """
        Basemodel.__init__(self)
        self.username = username
        self.email = email
        self.password = password
        self.lastLogin = str(lastLogin)
        self.createdon = str(createdon)
        self.updatedon = str(updatedon)
        self.status = status
        self.firstName = firstName
        self.lastName = lastName

    def register_user(self):

        """
        This method implements the following functionalitites :-
            1) Initialises the database connection client. If successful, proceed to step 2 else return as exception
            2) Checks if the email id already exists in the database. If it does, return, else, proceed to step 3.
            3) Inserts the form data into the database. If successful, return .

        """

        print ("In register_user block")

        try:
            if self.db.aceuser.find({"email": self.email}).count() >= 1:
                message = registration_messages["MSG_001"]
                return message
            else:
                userid = self.db.aceuser.insert({
                    "username": self.username,
                    "email": self.email,
                    "password": self.password,
                    "lastLogin": self.lastLogin,
                    "createdon": self.createdon,
                    "updatedon": self.updatedon,
                    "status": self.status,
                })

                self.db.aceuserprofile.insert({
                    "userId": userid,
                    "email": self.email,
                    "firstName": self.firstName,
                    "lastName": self.lastName,
                })
                message = registration_messages["MSG_002"]
                return message

        except Exception as e:
            #message = registration_messages["MSG_003"]
  
          return {"status" : "500", "message" : str(e)}
