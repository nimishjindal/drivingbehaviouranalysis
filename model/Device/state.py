"""

This file tries to implement the functionalities related to the device and sensor states in the database.
The file consists of the Role class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) get_device_state - Gets the state for a particular device
    3) update_device_state - Updates the state for a particular device
    4) get_sensor_state - Gets the state of a particular sensor in a particular device
    5) update_sensor_state - Updates the state of a particular sensor in a particular device

"""

from bson.objectid import ObjectId
from model.basemodel import Basemodel
from config.messages  import device_status_messages, device_status_updation_messages, sensor_status_messages\
    , sensor_status_updation_messages


class State(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None
        self.sensor_id = None
        self.new_state = None

    def get_device_state(self, device_id):
        self.device_id = device_id

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:  # Device exists in the database
            item = self.db.acedevices.find_one({"_id" : ObjectId(self.device_id)})

            if item.get("state"):
                message = device_status_messages["MSG_001"]
                message["state"] = item.get("state")
            else:
                message = device_status_messages["MSG_002"]

        return message

    def update_device_state(self, device_id, new_state):
        self.device_id = device_id
        self.new_state = new_state

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:  # Device exists in the database
            try :
                self.db.acedevices.update_one({'_id': ObjectId(self.device_id)},
                                             {'$set': {'state': self.new_state}})

                message = device_status_updation_messages["MSG_001"]

            except :
                message = device_status_messages["MSG_002"]

        return message

    def get_sensor_state(self, device_id, sensor_id):
        self.device_id = device_id
        self.sensor_id = sensor_id

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            item = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
            for record in item:
                for sensor in record["sensors"]:  # Fetch the registered sensors to a particular device
                    if sensor["sensor_id"] == self.sensor_id:  # Get the details of the required sensor
                        message = sensor_status_messages["MSG_001"]
                        message["state"] = sensor["state"]  # Fetch the status of the required sensor
                    else:
                        message = sensor_status_messages["MSG_002"]

        return message

    def update_sensor_state(self, device_id, sensor_id, new_state):
        self.device_id = device_id
        self.sensor_id = sensor_id
        self.new_state = new_state

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            if self.db.acedevices.update({'sensors.sensor_id': self.sensor_id}, {'$set': {'sensors.$.state':
                                                                                          self.new_state}}):
                message = sensor_status_updation_messages["MSG_001"]
            else:
                message = sensor_status_updation_messages["MSG_002"]

        return message