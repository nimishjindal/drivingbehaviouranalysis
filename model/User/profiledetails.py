"""

This file is used for implementing the registration part of the website.
The file consists of the Registration class which has the following methods :-
    1) __init__ - Works as a constructor to initialise the variables/parameters
    2) register_user - Has the code for interacting with the database and implementing the registration section

"""

from config.messages  import profile_messages
from model.basemodel import Basemodel
from bson import ObjectId

class ProfileDetails(Basemodel):

    def __init__(self):

        """
        This method initialises the parameters/variables
        """
        Basemodel.__init__(self)
        self.userId = None

    """"""

    def getProfile(self, user_id):

            """
            This method gets the user profile and returns the corresponding message

            :param user_id:
            :return:
            """

            print ("In Get Profile Block")
            self.userId = user_id
            if self.db.aceuserprofile.find({"userId": ObjectId(self.userId)}).count() >= 1:
                items = self.db.aceuserprofile.find({"userId": ObjectId(self.userId)})
                message = profile_messages["MSG_004"]
                for record in items:
                    p = {}
                    p["firstName"] = str(record["firstName"])
                    p["lastName"] = str(record["lastName"])
                    p["email"] = str(record["email"])
                    p["phone"] = str(record["phone"]) if 'phone' in record else ''
                    p["subscribed"] = record["subscribed"] if 'subscribed' in record else False
                    message["profileData"] = p
            else:
                message = profile_messages["MSG_005"]

            return message
