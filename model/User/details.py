"""
This file is used to get the details of a registered user

"""

import datetime
from bson.objectid import ObjectId
from config.messages  import user_search_messages
from model.basemodel import Basemodel


class Search(Basemodel):

    def __init__(self, user_id):

        """

        :param user_id:

        This method initialises the variables/parameters

        """

        Basemodel.__init__(self)
        self.user_id = user_id

    def search_user(self):

        """

        This method implements the following functionalities :-
            1)  Tries to search the user details -> If successful -> Return success message
            2)  If it fails -> Return failure message

        """

        print "In user_search block"

        if self.db.aceuser.find({"_id": ObjectId(self.user_id)}).count() >= 1:  # User exists in the database
            message = {}
            item = self.db.aceuser.find_one({"_id" : self.user_id})
            message["status"] = user_search_messages["MSG_001"]["status"]
            message["message"] = user_search_messages["MSG_001"]["message"]
            message["data"] = {}

            message["data"]["username"] = item.get("username")
            message["data"]["mobile"] = item.get("mobile")
            message["data"]["usertype"] = item.get("usertype")
            message["data"]["email id"] = item.get("email")

        else:
            message = {}
            message["status"] = user_search_messages["MSG_002"]["status"]
            message["message"] = user_search_messages["MSG_002"]["message"]

        return message

