"""

This file tries to implement the functionalities related to the device roles in the database.
The file consists of the Role class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) create_role - Creates the role for a particular device
    3) update_role - Updates the role for a particular device
    4) list_roles - Lists the role for a particular device

"""

from bson.objectid import ObjectId
from model.basemodel import Basemodel
from config.messages  import role_creation_messages, role_listing_messages, role_updation_messages, role_search_messages


class Role(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None
        self.role_code = None
        self.role_name = None
        self.role_value = None

    def create_role(self, role_code, user_id, device_id, role_name, role_value):

        """
        This method tries to create a role for a device in the database and returns the corresponding message

        :param role_code:
        :param user_id:
        :param device_id:
        :param role_name:
        :param role_value:
        :return:

        """

        print ("In create_role block")

        self.user_id = user_id
        self.device_id = device_id
        self.role_code = role_code
        self.role_name = role_name
        self.role_value = role_value
        role_data = {"code" : self.role_code, "name" : self.role_name, "value" : self.role_value}

        try:
            self.db.acedevices.update({'$and' : [{"_id" : ObjectId(self.device_id)}, {"userId" : ObjectId(self.user_id)}]},
                                      {'$push': {'roles': role_data}})
            message = role_creation_messages["MSG_001"]
        except Exception as e:
            print (e)
            message = role_creation_messages["MSG_002"]

        return message

    def update_role(self, device_id, role_code, new_role_name, new_role_value):

        """

        This method updates the role for a device and returns the corresponding message

        :param device_id:
        :param role_code:
        :param new_role_name:
        :param new_role_value:
        :return:

        """

        print ("In update_role block")

        self.device_id = device_id
        self.role_code = role_code
        self.role_name = new_role_name
        self.role_value = new_role_value

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:

            if self.db.acedevices.update({
                'roles.code': self.role_code
            },{'$set' : {'roles.$.name':self.role_name, 'roles.$.value':self.role_value}}):
                message = role_updation_messages["MSG_001"]
            else:
                message = role_updation_messages["MSG_002"]
        else:
            message = role_updation_messages["MSG_003"]

        return message

    def search_role(self, device_id, role_code):

        print ("In search_role block")

        self.device_id = device_id
        self.role_code = role_code
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            item = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
            message = role_search_messages["MSG_001"]
            message["role"] = []
            for record in item:
                for role in record["roles"]:
                    if role["code"] == self.role_code:
                        ro = {}
                        ro["name"] = str(role["name"])
                        ro["value"] = str(role["value"])
                        ro["code"] = str(role["code"])
                        message["role"].append(ro)
        else:
            message = role_listing_messages["MSG_002"]

        return message

    def list_roles(self, device_id):

        """

        This method lists the roles for a particular device and returns the corresponding message

        :param device_id:
        :return:

        """

        print ("In list_roles block")

        self.device_id = device_id
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            items = self.db.acedevices.find({"_id" : ObjectId(self.device_id)})
            message = role_listing_messages["MSG_001"]
            message["roles"] = []
            for record in items:
                for role in record["roles"]:
                    ro = {}
                    ro["name"] = str(role["name"])
                    ro["value"] = str(role["value"])
                    ro["code"] = str(role["code"])
                    message["roles"].append(ro)
        else :
            message = role_listing_messages["MSG_002"]

        return message