from model.basemodel import Basemodel
from config.messages  import device_registration_messages
from config import constants as const
from bson.objectid import ObjectId



class Columns(Basemodel):

        def __init__(self,user_id, device_id, sensor_id):

            """

            This method acts as a constructor

            """

            Basemodel.__init__(self)
            self.user_id = user_id
            self.device_id = device_id
            self.sensor_id = sensor_id
            self.key = str(self.user_id) + "_" + str(self.device_id) + "_" + str(self.sensor_id)

        def put_model(self,cols,model_name,n):

            print(cols)
            print ("In save columns block")

            try:
                self.db.models.insert({
                    "key" : self.key,
                    "name": model_name,
                    "cols": cols,
                    "n": n,
                    "trained" : "false"
                })

                message = {"status":200,"message":"Successfully saved model to db"}
                return message
            except Exception as e:
                print (e)
                message = {"status":300,"message":str(e)}
                return message
        
        def update_model(self,trained,model_name):
            try:
                model = self.db.models.find_one({"key" : self.key , "name": model_name})
                
                         
                model_id = model["_id"]

                if self.db.models.update_one({'_id': ObjectId(model_id)},
                                             {'$set': {'trained': trained}}):
            
                    message = {"status": 200, "message":"saved status"}
                else:
                    message = {"status": 300, "message":"couldnt update"}

            except Exception as e :
                message = {"status": 300, "message":"some exception" , "error" : str(e)}

            return message

        def get_models(self):

            print ("In columms columns block")
            try:
                models = self.db.models.find({"key" : self.key})
            
                message = {}
                message["count"] = models.count()

                if models.count() >= 1:  # model exists in the database
                    message["models"] = []
                    for record in models:
                        d = {}
                        d["_id"] = str(record["_id"])
                        d["name"] = str(record["name"])
                        d["n"] = str(record["n"])
                        d["cols"] = record["cols"]
                        d["trained"] = str(record["trained"])

                        message["models"].append(d)
            
                    message["status"] = 200
                    message["message"] = "found models"
                else:
                    message["status"] = 200
                    message["message"] = "no models found"
             
            except Exception as e:
                message = {"status": 300, "message":"some exception" , "error" : str(e)}
            return message