

function view_alert(msg) {

    str = '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
    str += msg
    str += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
    str += '<span aria-hidden="true"> &times; </span>'
    str += '</button >'
    str += '</div >'

    document.getElementById('alert').innerHTML = str

}


(function ($) { //testing js


    $("form").submit(function (event) {

        event.preventDefault();

        if ($("form").attr("id") == "login") {
            var formData = {
                'email': $('input[name=email]').val(), //for get email
                'password': $('input[name=password]').val() //for get pass
            };

            URL = "/user/login"
            err_msg = "Incorrect username or password"
            redir = "/profile"
        }

        $prev_btn = $('input[name=btn]').val()

        $('input[name=btn]').prop("value", "Please wait...");
        $('input[name=btn]').prop("disabled", "disabled");

        $.ajax({
            url: URL,
            type: "get",
            data: formData,
            success: function (d) {

                if (d["status"] == 200) {
                    view_alert(d["message"]);
                    window.location = redir;
                }
                else {

                    $('input[name=btn]').prop("value", $prev_btn);
                    $('input[name=btn]').removeAttr("disabled");

                    view_alert(d["message"])

                }
            }
        });

    });


})(jQuery);

