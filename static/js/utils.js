
function viewButton(parent) {
    if (editmode == 0) {
        parent.querySelector("#edit").style.display = 'block'
    }
}

function hideButton(parent) {
    if (editmode == 0) {
        parent.querySelector("#edit").style.display = 'none'
    }
}

function loadForm(parent_name) {

    editmode = 1

    parentDIV = document.getElementById(parent_name)
    label = parentDIV.children[0].innerHTML;
    prev_view = parentDIV.children[1];
    form = parentDIV.children[2];
    prev = parentDIV.querySelector("#value").innerHTML;
    prev_view.style.display = 'none'
    form.style.display = 'block'
    form.querySelector("input").setAttribute("value", prev)
}

function create_device_tab(name) {

    original_name = name

    name = name.replace(" ","_").replace("'","_")

    str = ""
    
    str = `<li class="nav-item active">
            <a class="nav-link" id=" `+ name + `-tab" data-toggle="tab" href="#` + name + `" role ="tab" aria-controls="` + name + `" aria-selected="false">
                `+original_name+`
            </a>
        </li>`

    return str
}

function view_alert(msg) {

    str = '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">'
    str += '<span class="badge badge-pill badge-success"><i class="fa fa-check-square-o"></i></span> '
    str += msg
    str += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
    str += '<span aria-hidden="true"> &times; </span>'
    str += '</button >'
    str += '</div >'

    document.getElementById('alert').innerHTML = str

}

function create_device_page(device) {

    var name = device["device"].replace(" ", "_").replace("'","_")
    var id = device["_id"]
    var type = device["devicetype"]
    var model = device["model"]
    var status = device["enabled"]
    if ("sensors" in device)
        var sensors = device["sensors"]
    else
        var sensors = {}

    str = '<p>'

    str += '<div class="tab-pane fade show" id="' + name + '" role="tabpanel" aria-labelledby="' + name + '-tab">'

    str +=      '<div class="row" >'
    str +=          `<div class="col-lg-3">` + type+  `</div>`
    str +=          `<div class="col-lg-3">`+model+ `</div>`
    str +=          `<div class="col-lg-3">`
    str +=              `<label class="switch">`
    if (status == 'true' || status == 'True'){
        str +=              `<input type="checkbox" id="status" name="enabled" checked>`
    }    
    else{
        str +=              `<input type="checkbox" id="status" name="enabled">`
    }
    str +=                  `<span class="slider round"></span>`
    str +=              `</label>`
    str +=          `</div>`
    str +=          `<div class="col-lg-3">`
    str +=              `<button class="btn btn-danger" onclick="delete_device('`+id+`')" >`
    str +=              `<i  class="fa fa-times-circle"></i>`
    str +=              ` Delete`
    str +=              '</button>'
    str +=          `</div>`
    str +=       `</div>`
    str +=    `<hr>`

    if (sensors.length == 0)
        str += "No sensors available"

    sensors.forEach(function (item, index) {
        //add_option(item["name"], item["id"], "select_s")
 

        str += `<div class= "row">
                    <div class="col-lg-3">
                        `+item["name"]+`
                    </div>
                    <div class="col-lg-3">
                        `+item["type"]+`
                    </div>
                    <div class="col-lg-3">
                      `+item["state"]+`
                    </div>
                    <div class="col-lg-3">
                        <a href="/devicedata/` + id + `/`+item["id"]+`">
                          <button type="button" class="btn btn-success" >
                                 <i class="fa fa-magic"> </i>
                                    View Stats
                                 </button >
                        </a >
                     </div>
                </div>
                <hr>`
    })
    
    str += '</p>'

    return str
}