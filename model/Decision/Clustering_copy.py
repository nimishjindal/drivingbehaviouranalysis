import numpy as np
from sklearn.cluster import KMeans
from sklearn import preprocessing
from model.Device.data import Data
import pandas as pd
import time
import os, sys
from config import constants
from model.basemodel import Basemodel
from common.utils import get_all_columns
import pickle
from model.Device.columns import Columns



def get_dir(key):

  path = "static/images/"

  return path

class Clustering(Data):

  def __init__(self,user_id, device_id, sensor_id):
      
      """
      This method acts as a constructor

      """
      Data.__init__(self,user_id, device_id, sensor_id)
      self.user_id = user_id
      self.device_id = device_id
      self.sensor_id = sensor_id
      self.key = str(self.user_id) + "_" + str(self.device_id) + "_" + str(self.sensor_id)

  def predict(self,name="",cols=[],length=10000,pretrained="false"):
 
    print ("prediction")

    try:
      
      message = self.fetch_data("now",length)
      if message["status"] != 200:
                return message
      data = message["data_obd"]
   
      df = pd.DataFrame(data)
      if pretrained=="true":
        cols  = ["Engine_speed","Vehicle_speed","Absolute_throttle_position","Calculated_LOAD_value"] #overwritten cols for pretrained model 
        filename = 'data/pickles/Clustering/pretrained.pickle'
      else:
        filename = 'data/pickles/Clustering/'+self.get_collection_name()+"_"+name+'.pickle'

      df.drop(df.columns.difference(cols), 1, inplace=True)

      X = np.array(df)
      X = preprocessing.scale(X)


      clf = pickle.load(open(filename, 'rb'))
     
      print("working till here")
      
      prediction_list = []
      for i in range(len(X)):
          predict_me = np.array(X[i].astype(float))
          predict_me = predict_me.reshape(-1, len(predict_me))
          prediction = clf.predict(predict_me)
          prediction_list += [int(prediction[0])]

      return {'Status' : 200, 'message' : 'successfully created graph' ,"predictions" : prediction_list, "data":data}

    except Exception as e:
      print(e)
      return {'status' : 300 , 'message' : str(e)}
  
  def cluster(self,name,cols,n,length=9999,period="now",pretrained="false"):
    """
        #function for clustering the data points given on the given cols, this function will give 'n' clusters
        #length, period, cols,n
    """
    print ("train the model")

    try:
      
      message = self.fetch_data(period,length)
      if message["status"] != 200:
                print("error in fetching")
                return message
      data = message["data_obd"]

      df = pd.DataFrame(data)
      df.drop(df.columns.difference(cols), 1, inplace=True)

      X = np.array(df)
      X = preprocessing.scale(X)

      clf = KMeans(n_clusters=int(n))
      clf.fit(X)

      if pretrained=="true":
          filename = 'data/pickles/Clustering/pretrained.pickle'
      else:
          filename = 'data/pickles/Clustering/'+self.get_collection_name()+"_"+name+'.pickle'

      pickle.dump(clf, open(filename, 'wb'))

      print("trained")
      
      cols_db = Columns(self.user_id, self.device_id, self.sensor_id)
      message = cols_db.update_model("true",name)

      if(message["status"] == 200 ):
          return {'status' : 200, 'message' : 'successfully saved model'}
      else:
          return message


    except Exception as e:
      print(e)
      return {'status' : 300 , 'message' : str(e)}