"""

This file tries to implement the functionalities related to the device actions in the database.
The file consists of the Action class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) create_action - Creates the action for a particular device
    3) update_action - Updates the action for a particular device
    4) list_actions - Lists the actions for a particular device

"""

import json
from bson import json_util
from bson.objectid import ObjectId
from model.basemodel import Basemodel
from config.messages  import action_creation_messages, action_listing_messages, action_updation_messages, \
    action_search_messages


class Action(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None
        self.action_code = None
        self.action_name = None
        self.action_type = None
        self.action_status = None

    def create_action(self, user_id, device_id, action_code, action_name, action_type, action_status):

        """

        This method tries to create an action for a device in the database and returns the corresponding message

        :param user_id:
        :param device_id:
        :param action_code:
        :param action_name:
        :param action_type:
        :param action_status:
        :return:

        """

        print ("In create_action block")

        self.user_id = user_id
        self.device_id = device_id
        self.action_code = action_code
        self.action_name = action_name
        self.action_type = action_type
        self.action_status = action_status
        action_data = {"code" : self.action_code, "name" : self.action_name,
                       "action" : self.action_type, "status" : self.action_status,}

        try :
            self.db.acedevices.update({'$and' : [{"_id": ObjectId(self.device_id)},
                                                 {"userId": ObjectId(self.user_id)}]},
                                      {'$push': {'actions': action_data}})
            message = action_creation_messages["MSG_001"]
        except Exception as e:
            print (e)
            message = action_creation_messages["MSG_002"]

        return message

    def update_action(self, device_id, action_code, new_action_status, new_action_type, new_action_name):

        """

        This method updates the action for a device and returns the corresponding message

        :param device_id:
        :param action_code:
        :param new_action_status:
        :param new_action_type:
        :param new_action_name:
        :return:

        """

        print ("In update_action block")

        self.device_id = device_id
        self.action_code = action_code
        self.action_status = new_action_status
        self.action_type = new_action_type
        self.action_name = new_action_name

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:

            if self.db.acedevices.update({
                'actions.code': self.action_code
            },{'$set': {'actions.$.name':self.action_name,'actions.$.status':self.action_status,
                         'actions.$.action' : self.action_type}}):
                message = action_updation_messages["MSG_001"]
            else:
                message = action_updation_messages["MSG_002"]
        else:
            message = action_updation_messages["MSG_003"]

        return message

    def search_action(self, device_id, action_code):

        print ("In search_action block")

        self.device_id = device_id
        self.action_code = action_code
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            item = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
            message = action_search_messages["MSG_001"]
            message["action"] = []
            for record in item:
                for action in record["actions"]:
                    if action["code"] == self.action_code:
                        ac = {}
                        ac["code"] = action["code"]
                        ac["name"] = action["name"]
                        ac["status"] = action["status"]
                        ac["action"] = action["action"]
                        message["action"].append(ac)
        else:
            message = action_search_messages["MSG_002"]

        return message

    def list_actions(self, device_id):

        """

        This method lists the actions for a particular device and returns the corresponding message

        :param device_id:
        :return:

        """

        print ("In list_actions block")

        self.device_id = device_id
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            items = self.db.acedevices.find({"_id" : ObjectId(self.device_id)})
            message = action_listing_messages["MSG_001"]
            message["actions"] = []
            for record in items:
                for action in record["actions"]:
                    ac = {}
                    ac["code"] = action["code"]
                    ac["name"] = action["name"]
                    ac["status"] = action["status"]
                    ac["action"] = action["action"]
                    message["actions"].append(ac)
        else :
            message = action_listing_messages["MSG_002"]

        return message