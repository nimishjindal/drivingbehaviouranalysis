"""

This file tries to implement the device updation functionality of the website.
The file consists of the U_Device class which contains the following methods :-
    1) __init__ - Initialises the variables/parameters
    2)  update - Implements the device updation functionality

"""


import json
from model.basemodel import Basemodel
from config.messages  import device_update_messages
from bson.objectid import ObjectId
from bson import json_util
import datetime


class ssh_U_Device(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None
        self.sshuser = None
        self.ipaddress = None
        self.port = None

    def update(self, user_id, device_id, ipaddress, sshuser, port):

        """

        This method implements the following functionality :-
            1)  Tries to update the device details in the database
            2)  Returns the corresponding message

        :param device_id:
        :param device_name:
        :return:

        """

        print ("In update device block using SSH")

        self.user_id = user_id
        self.device_id = device_id
        self.ipaddress = ipaddress
        self.sshuser = sshuser
        self.port = port

        # print "In device details updation block - After ssh config file execution"

        if self.db.acedevices.update_one({'_id': ObjectId(self.device_id)},
                                         {'$set': {'ipaddress' : self.ipaddress,
                                                   'sshuser' : self.sshuser,
                                                   'port' : self.port}}):
            message = device_update_messages["MSG_001"]
        else:
            message = device_update_messages["MSG_002"]
        return message