import datetime
from flask import Blueprint, request, jsonify, session
from flask import current_app as app
from common.utils import validate_token, check_auth, get_userid, get_length, download_file
from model.Device.registration import Registration_D
from model.Device.list import Device
from model.Device.update import U_Device
from model.Device.roles import Role
from model.Device.rules import Rule
from model.Device.actions import Action
from model.Device.sensors import Sensor
from model.Device.delete import Delete
from model.Device.search import Search
from model.Device.ssh_update import ssh_U_Device
from model.Device.data import Data
from model.Device.state import State
from config.constants import cols

#from pexpect import pxssh
import boto3

deviceservice = Blueprint('deviceservice', __name__, url_prefix='/device')

# ------------------------------ Device Registration ------------------------------

# Route to handle the device registration page


@deviceservice.route('/register', methods=['GET', 'POST'])
def register():
    try:
        ##token = request.headers["Authorization"]
        #response = check_auth()
        response = check_auth()
        app.logger.info(response)
        if response["status"] == 1 :
            user_id = get_userid(response['email'])
            device_name = request.args['device']
            ssh_user =  request.args['sshuser']
            device_type = request.args['devicetype']
            device_code = request.args['devicecode']
            ip_address = request.args['ipaddress']
            ssh_password = request.args['password']
            port = request.args['port']
            device_macid = request.args['macid']
            device_model = request.args['model']
            network_id = request.args['network']
            status = request.args['enabled']
            access_point = request.args['accesspoint']
            rules = []
            actions = '' #request.args['actions']
            datapoint = "" #request.args['datapoint']
            data_endpoints = "" #request.args['dataendpoint']

            created_on = str(datetime.datetime.utcnow())
            updated_on = str(datetime.datetime.utcnow())
            last_login_on = str(datetime.datetime.utcnow())
            longitude = "longitude"
            latitude = "latitude"
            network_type = "wifi"
            roles = []
            sensors = []
            state = "0"

            d = Registration_D()
            message = d.register_device(user_id, device_name, device_type, device_code, device_macid, device_model,
                                        ssh_user, ssh_password, ip_address, port, access_point, network_id, created_on,
                                        updated_on, last_login_on, status, actions, datapoint, data_endpoints,
                                        longitude, latitude, network_type, roles, rules, sensors)
            app.logger.info(message["message"])
            message["device_name"] = device_name
            return jsonify(message)
        else:
            app.logger.info(message["message"])
            return jsonify(response)
    except Exception as e:
        app.logger.info(e)
        return jsonify({'status':300 , 'message':str(e)})


# ------------------------------ Device Listing ------------------------------

# Route to handle the device listing page


@deviceservice.route('/list', methods=['GET', 'POST'])
def list():
    ##token = request.headers["Authorization"]
    #response = check_auth()
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        #print "User ID : " + str(user_id)
        device = Device()
        message = device.list_devices(user_id)

        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device Updation ------------------------------

# Route to handle the device updation page


@deviceservice.route('/update/<device_id>', methods=["GET", "POST"])
def update(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])

        device = request.json["DeviceForm"]['device']
        sshuser =  request.json["DeviceForm"]['sshuser']
        devicetype = request.json["DeviceForm"]['devicetype']
        devicecode = request.json["DeviceForm"]['devicecode']
        ipaddress = request.json["DeviceForm"]['ipaddress']
        password = request.json["DeviceForm"]['password']
        port = request.json["DeviceForm"]['port']
        macid = request.json["DeviceForm"]['macid']
        model = request.json["DeviceForm"]['model']
        networkid = request.json["DeviceForm"]['network']
        enabled = request.json["DeviceForm"]['enabled']
        accesspoint = request.json["DeviceForm"]['accesspoint']
        actions = request.json["DeviceForm"]['actions']
        rules = request.json["DeviceForm"]['rules']
        datapoint = request.json["DeviceForm"]['datapoint']
        dataendpoints = request.json["DeviceForm"]['dataendpoint']
        # state = request.json["DeviceForm"]["state"]
        # device = request.json["device"]
        # devicetype = request.json["devicetype"]
        # devicecode = request.json["devicecode"]
        # sshuser = request.json["sshuser"]
        # password = request.json["password"]
        # ipaddress = request.json["ipaddress"]
        # port = request.json["port"]
        # macid = request.json["macid"]
        # model = request.json["model"]
        # networkid = request.json["networkid"]
        # enabled = request.json["enabled"]
        # accesspoint = request.json["accesspoint"]
        # datapoint = request.json["datapoint"]
        # dataendpoints = request.json["dataendpoints"]
        updatedon = datetime.datetime.utcnow()
        longitude = "longitude"
        latitude = "latitude"
        network_type = "wifi"

        d = U_Device()
        message = d.update(device_id, device, devicetype, devicecode, sshuser, password, ipaddress, port, macid, model,
               networkid, enabled, accesspoint, datapoint, dataendpoints, updatedon, longitude, latitude, network_type)

        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device Search ------------------------------

# Route to handle the device search request


@deviceservice.route('/search/<device_id>', methods=["GET", "POST"])
def search(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        s = Search()
        message = s.list_device(device_id)

        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device Deletion ------------------------------

# Route to handle the device deletion page


@deviceservice.route('delete_device/<device_id>', methods=["GET", "POST"])
def delete_device(device_id = None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        d = Delete()
        message = d.del_device(user_id, device_id)

        return jsonify(message)
    else:
        return jsonify(response)

    return device_id

# ------------------------------ Device state check ------------------------------

@deviceservice.route('/getdevicestate',  methods=["GET", "POST"])
def getdevicestate():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        device_id = request.json["device_id"]
        st = State()
        message = st.get_device_state(device_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device state updation ------------------------------

@deviceservice.route('/updatedevicestate', methods=["GET", "POST"])
def updatedevicestate():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        device_id = request.json["device_id"]
        new_state = request.json["new_state"]
        st = State()
        message = st.update_device_state(device_id, new_state)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device Role creation ------------------------------

# Route to handle the role creation page


@deviceservice.route('/createrole/<device_id>', methods=["GET", "POST"])
def createrole(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        role_length = get_length(device_id, "roles")
        role_code = "Device Role " + str(role_length)
        role_name = request.json["role_name"]
        role_value = request.json["role_value"]
        r = Role()
        message = r.create_role(role_code, user_id, device_id, role_name, role_value)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device Role updation ------------------------------

# Route to handle the role updation page


@deviceservice.route("/updaterole/<device_id>", methods = ["GET", "POST"])
def updaterole(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        role_code = request.json["role_code"]
        new_role_name = request.json["role_name"]
        new_role_value = request.json["role_value"]
        r = Role()
        message = r.update_role(device_id, role_code, new_role_name, new_role_value)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device role search ------------------------------

# Route to handle the role searching request


@deviceservice.route("/searchrole/<device_id>", methods=["GET", "POST"])
def searchrole(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        role_code = request.json["role_code"]
        r = Role()
        message = r.search_role(device_id, role_code)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device role listing ------------------------------

# Route to handle the role listing page


@deviceservice.route('/listroles/<device_id>', methods=["GET", "POST"])
def listroles(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        r = Role()
        message = r.list_roles(device_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device rule creation ------------------------------

# Route to handle the rule creation page


@deviceservice.route('/createrule/<device_id>', methods=["GET", "POST"])
def createrule(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        rule_length = get_length(device_id, "rules")
        rule_code = "DR" + str(rule_length)
        rule_name = request.json["RuleForm"]["name"]
        rule_type = request.json["RuleForm"]["type"]
        rule_value = request.json["RuleForm"]["val"]
        rule_attribute = request.json["RuleForm"]["attrib"]
        rule_status = request.json["RuleForm"]["status"]
        rule_desc = request.json["RuleForm"]["desc"]
        rule_condition = request.json["RuleForm"]["condition"]
        rule_actions = request.json["RuleForm"]["actions"]
        rule_param = request.json["RuleForm"]["parameter"]
        rule_msg= request.json["RuleForm"]["msg"]
        r = Rule()
        message = r.create_rule(user_id,device_id, rule_code, rule_name, rule_value, rule_attribute, rule_status,
                                rule_actions, rule_condition, rule_desc, rule_param, rule_type, rule_msg)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device rule updation ------------------------------

# Route to handle the rule updation page


@deviceservice.route("/updaterule/<device_id>", methods = ["GET", "POST"])
def updaterule(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        #rule_length = get_length(device_id, "rules")
        rule_code = request.json["RuleForm"]["code"]
        rule_name = request.json["RuleForm"]["name"]
        rule_type = request.json["RuleForm"]["devicetype"]
        rule_value = request.json["RuleForm"]["val"]
        rule_attribute = request.json["RuleForm"]["attrib"]
        rule_status = request.json["RuleForm"]["status"]
        rule_desc = request.json["RuleForm"]["desc"]
        rule_condition = request.json["RuleForm"]["condition"]
        rule_actions = request.json["RuleForm"]["act"]
        rule_param = request.json["RuleForm"]["parameter"]
        r = Rule()
        message = r.update_rule(device_id, rule_code, rule_name, rule_value, rule_attribute, rule_status,
                                rule_actions,rule_condition,rule_desc,rule_param)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device rule search ------------------------------

# Route to handle the rule searching request


@deviceservice.route("/searchrule/<device_id>", methods=["GET", "POST"])
def searchrule(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        rule_code = request.json["rule_code"]
        r = Rule()
        message = r.search_rule(device_id, rule_code)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device rule listing ------------------------------

# Route to handle the rule listing page


@deviceservice.route('/listrules/<device_id>', methods=["GET", "POST"])
def listrules(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        r = Rule()
        message = r.list_rules(device_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device action creation ------------------------------

# Route to handle the action creation page


@deviceservice.route('/createaction/<device_id>',
                     methods=["GET", "POST"])
def createaction(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        action_length = get_length(device_id, "actions")
        action_code = "Device Action " + str(action_length)
        action_name = request.json["action_name"]
        action_type = request.json["action_type"]
        action_status = request.json["action_status"]
        a = Action()
        message = a.create_action(user_id, device_id, action_code, action_name, action_type, action_status)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device action updation ------------------------------

# Route to handle the action updation page


@deviceservice.route("/updateaction/<device_id>", methods = ["GET", "POST"])
def updateaction(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        action_code = request.json["action_code"]
        new_action_status = request.json["action_status"]
        new_action_type = request.json["action_type"]
        new_action_name = request.json["action_name"]
        a = Action()
        message = a.update_action(device_id, action_code, new_action_status, new_action_type, new_action_name)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device action search ------------------------------

# Route to handle the action search request


@deviceservice.route("/searchaction/<device_id>", methods=["GET", "POST"])
def searchaction(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        action_code = request.json["action_code"]
        a = Action()
        message = a.search_action(device_id, action_code)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device action listing ------------------------------

# Route to handle the action listing page


@deviceservice.route('/listactions/<device_id>', methods=["GET", "POST"])
def listactions(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        a = Action()
        message = a.list_actions(device_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device sensor creation ------------------------------

# Route to handle the sensor creation page


@deviceservice.route('/createsensor/<device_id>', methods=["GET", "POST"])
def createsensor(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        sensor_length = get_length(device_id, "sensors")
        sensor_code = "Device Sensor " + str(sensor_length)
        sensor_name = request.json["DeviceForm"]["name"]
        sensor_type = request.json["DeviceForm"]["type"]
        sensor_gpio = request.json["DeviceForm"]["gpio"]
        sensor_comm_protocol = request.json["DeviceForm"]["comport"]
        timestamp = datetime.datetime.utcnow()
        s = Sensor()
        message = s.create_sensor(user_id, device_id, sensor_code, sensor_name, sensor_type, sensor_gpio,
                                  sensor_comm_protocol,timestamp)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device sensor updation ------------------------------

# Route to handle the sensor updation page


@deviceservice.route("/updatesensor/<device_id>", methods=["GET", "POST"])
def updatesensor(device_id=None):
    ##token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        sensor_code = request.json["DeviceForm"]["code"]
        new_sensor_name = request.json["DeviceForm"]["name"]
        new_sensor_type = request.json["DeviceForm"]["type"]
        new_sensor_gpio = request.json["DeviceForm"]["gpio"]
        new_comm_protocol = request.json["DeviceForm"]["comport"]
        sensor_id = request.json["DeviceForm"]["sensor_id"]
        r = Sensor()
        message = r.update_sensor(device_id, sensor_code, sensor_id, new_sensor_name, new_sensor_type, new_sensor_gpio,
                                  new_comm_protocol)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device sensor search ------------------------------

# Route to handle the sensor search request


@deviceservice.route("/searchsensor/<device_id>", methods=["GET", "POST"])
def searchsensor(device_id=None):
    ##token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        sensor_code = request.json["code"]
        r = Sensor()
        message = r.search_sensor(device_id, sensor_code)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device sensor listing ------------------------------

# Route to handle the sensor listing page


@deviceservice.route('/listsensors/<device_id>', methods=["GET", "POST"])
def listsensors(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        r = Sensor()
        message = r.list_sensors(device_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Sensor state check ------------------------------

@deviceservice.route('/getsensorstate', methods=["GET", "POST"])
def getsensorstate():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        device_id = request.json["device_id"]
        sensor_id = request.json["sensor_id"]
        st = State()
        message = st.get_sensor_state(device_id, sensor_id)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ Device state updation ------------------------------

@deviceservice.route('/updatesensorstate', methods=["GET", "POST"])
def updatesensorstate():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        device_id = request.json["device_id"]
        sensor_id = request.json["sensor_id"]
        new_state = request.json["new_state"]
        st = State()
        message = st.update_sensor_state(device_id, sensor_id, new_state)
        return jsonify(message)
    else:
        return jsonify(response)


# ------------------------------ SSH file configuration ------------------------------

# Route to handle the ssh config file download process


@deviceservice.route('/sshconfig/<device_id>', methods=["GET", "POST"])
def sshconfig(device_id=None):
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        file_name = str(user_id) + "_" + str(device_id) + ".sh"
        message = download_file(file_name)
        return jsonify({"path" : message})
    else:
        return jsonify(response)


# ------------------------------ Device details updation after ssh connection ------------------------------

# Route to update device details after successful SSH


@deviceservice.route('/sshupdate', methods=["GET", "POST"])
def sshupdate():
    user_id = request.json["user_id"]
    device_id = request.json["device_id"]
    ipaddress = request.json["ipaddress"]
    username = request.json["username"]
    port = request.json["port"]
    u = ssh_U_Device()
    message = u.update(user_id, device_id, ipaddress, username, port)
    return jsonify(message)


# ------------------------------ Device sensor deployment ------------------------------

# Route to deploy the sensor and execute the code for pushing the data -

"""
@deviceservice.route('/deploysensor', methods=["GET", "POST"])
def deploysensor():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:

        # Fetch the device details

        ipaddress = request.json["ipaddress"]
        username = request.json["username"]
        password = request.json["password"]
        port = request.json["port"]
        count = request.json["count"]
        interval = request.json["interval"]

        s = pxssh.pxssh()

        if not s.login(ipaddress, username, password, port=port):
            print ("login failed")
           # print str(s)
        else: # Login is possible only if port forwarding is enabled.
            print ("login successful")
            s.sendline('wget https://s3-us-west-2.amazonaws.com/acecoredemo/dht11.py')
            s.prompt()
            s.sendline('wget https://s3-us-west-2.amazonaws.com/acecoredemo/Temperature.py')
            s.prompt()
            s.sendline('sudo python Temperature.py ' + str(count) + " " + str(interval))
            s.logout()

"""
# Route to handle the data push method from the sensor to the S3 Bucket - For dht sensor


# @deviceservice.route('/pushdata', methods=["GET", 'POST'])
# def pushdata():
#     timestamp = request.json["timestamp"]
#     temperature = request.json["temperature"]
#     humidity = request.json["humidity"]
#     dynamodb = boto3.resource('dynamodb')
#     table = dynamodb.Table('Temperature_Data')
#
#     if table.put_item(
#         Item={
#             'Timestamp' : timestamp,
#             'Temperature' : temperature,
#             'Humidity' : humidity
#         }
#     ):
#
#         return jsonify({"message" : "Data entered successfully"})


# ------------------------------ Data push ------------------------------

#  Route to put data from the sensor - OBD  to the S3 bucket

@deviceservice.route('/putdata', methods=["GET", "POST"])
def putdata():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
    #if 1 == 1:
        user_id = get_userid(response["email"])
        #user_id = 83993

        device_id = request.args.get("device_id")
        sensor_id = request.args.get("sensor_id")
        length = int(request.args.get("length"))
        d = Data(user_id, device_id, sensor_id )

        message = d.putdata(length)
        
        return jsonify(message)


# ------------------------------ Data fetch ------------------------------

#  Route to get data from the S3 bucket
@deviceservice.route('/countcsv', methods=["GET", "POST"])
def count():

    response = check_auth()
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        device_id = request.args.get("device_id")
        sensor_id = request.args.get("sensor_id")

        d = Data(user_id, device_id, sensor_id)
        message = d.CSV_length()
        return jsonify(message)

@deviceservice.route('/getdata', methods=["GET", "POST"])
def getdata():
    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
    #if 1 == 1:

        user_id = get_userid(response["email"])
        #user_id = 83993
        device_id = request.args.get("device_id")
        sensor_id = request.args.get("sensor_id")

        period = request.args.get("period")
        length = request.args.get("length")

        d = Data(user_id, device_id, sensor_id )
        message = d.fetch_data(period,length)

        return jsonify(message)

@deviceservice.route('/loadcsv', methods=["GET", "POST"])
def load():

    
    #response = check_auth()
    if 1==1: #response["status"] == 1:
        #user_id = get_userid(response["email"])



        start = int(request.args["start"])
        period = int(request.args["period"])

        d = Data()
        #cols= request.args("cols")
        message = d.load(start,period,cols)
        #print (jsonify(message))
        return jsonify(message)