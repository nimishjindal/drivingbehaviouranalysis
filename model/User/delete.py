"""

This file implements the user deletion functionality

The file consists of the Delete class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) delete_user - Checks for user existence and then performs deletion


"""

from config.messages  import user_deletion_messages
from model.basemodel import Basemodel
from bson.objectid import ObjectId


class Delete(Basemodel):

    def __init__(self):

        """

        This method acts as a constructor and initialises the parameters obtained from the form

        """

        Basemodel.__init__(self)
        self.user_id = None

    def delete_user(self, user_id):

        """

        :param user_id:
        :return:

        This method implements the following functionalities :-
            1)  Checks whether the user exists in the database
            2)  If user exists, performs deletion
                a)  If successful -> Returns success message
                b)  Else -> Returns failure message

        """

        self.user_id = user_id
        if self.db.aceuser.find({"_id" : ObjectId(self.user_id)}).count() >= 1:  # User exists in the database
            # print "In user deletion block"
            self.db.aceuser.remove({"_id" : ObjectId(self.user_id)})
            message = user_deletion_messages["MSG_001"]
        else:  # User with the given user_id does not exist in the database
            message = user_deletion_messages["MSG_002"]
        return message

