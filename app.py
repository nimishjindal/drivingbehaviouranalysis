from flask import Flask,render_template,Blueprint
import logging
from flask_cors import CORS
from flask import session
#import json
from Services.userservice import userservice
from Services.deviceservice import deviceservice
from Services.lexservice import lexservice
from Services.controllerservice import controller
from Services.machinelearningservice import ml
#import model.Decision.Clustering as cl

from config import constants as const

app = Flask(__name__)
app.secret_key = "SECRET_KEY"
CORS(app)
app.debug = True

# Route to handle the User related request
app.register_blueprint(userservice)
app.register_blueprint(deviceservice)

app.register_blueprint(controller)

app.register_blueprint(ml)



@app.errorhandler(404)
def handle_400_error(e):
    code = e.code
    return render_template('error.html' , code = code , description = e.description), e.code

@app.errorhandler(500)
def handle_500_error(e):
    code = 500
    return render_template('error.html' , code = code , description = e.description), code




if __name__ == "__main__":
    
    try:
        app.run(threaded=True)
    except Exception as e:
        print (str(e))
