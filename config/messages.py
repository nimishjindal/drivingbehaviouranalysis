#fetch_data messages
fetch_messages = {
    "MSG_001" : {"status": 200, "message" : "successfully fetched"},
    "MSG_002" : {"status": 300, "message" : "no more data available to be fetched"},
    "MSG_error" : {"status": 300, "message" : "some exception occured"}
}

# csv fetch messages 
csv_messages = {
    "MSG_001" : {"status" : 200, "message" : "Successfully pulled data from CSV"},
    "MSG_002" : {"status" : 300, "message" : "some error occured"}
}

put_data_messages = {
    "MSG_001" : {"status" : 200, "message" : "Data pushed successfully"},
    "MSG_error" : {"status" : 300, "message" : "some exception"},
    "MSG_002" : {"status" : 300, "message" : "no more data in csv"}
}

# Token validation messages

token_validation_message = {
    "MSG_001" : {"value" : 1, "message" : "User authenticated successfully"},
    "MSG_002" : {"value" : 2, "message" : "Token time has expired. Kindly login again"},
    "MSG_003" : {"value" : 3, "message" : "The token does not match. Kindly try again"},
    "MSG_004" : {"value" : 4, "message" : "The user does not exist in the database"}
}


# User registration messages

registration_messages = {
    "MSG_001" : {"status" : 300, "message" : "This email already exists"},
    "MSG_002" : {"status" : 200, "message" : "You have successfully registered"},
    "MSG_003" : {"status" : 300, "message" : "Registration failed"},
    "MSG_004" : {"status" : 300, "message" : "Password does not match"}
}


# User's password resetting messages

password_messages = {
    "MSG_001" : {"status" : 200, "message" : "A link has been sent on your registered email ID"},
    "MSG_002" : {"status" : 300, "message" : "Sorry this email is not registered with our system"},
    "MSG_003" : {"status" : 300, "message" : "Passwords do not match"},
    "MSG_004" : {"status" : 200, "message" : "Successfully updated the password"},
    "MSG_005" : {"status" : 300, "message" : "Could not update the password"}
}

profile_messages = {
    "MSG_001" : {"status" : 300, "message" : "This user does not exists"},
    "MSG_002" : {"status" : 200, "message" : "Profile updated successfully"},
    "MSG_003" : {"status" : 300, "message" : "Profile updation failed"},
    "MSG_004" : {"status" : 200, "message" : "Profile Details fetched Successfully"},
    "MSG_005" : {"status" : 300, "message" : "Could not fetch profile details"}
}

# User login messages

login_messages = {
    "MSG_001" : {"status" : 200, "message" : "Check OTP on your registered email ID"},
    "MSG_002" : {"status" : 300, "message" : "User not found - Incorrect password"},
    "MSG_003" : {"status" : 300, "message" : "User not found - Invalid email ID"},
    "MSG_004" : {"status" : 300, "message" : "Invalid OTP"}
}


# User sign out messages

signout_messages = {
    "MSG_001" : {"status" : 200, "message" : "You have successfully signed out"},
    "MSG_002" : {"status" : 300, "message" : "Failed to signout"}
}


# User details updation messages

update_messages = {
    "MSG_001" : {"status" : 200, "message" : "Successfully updated the user details"},
    "MSG_002" : {"status" : 300, "message" : "Could not update the user details"}
}

# User search messages

user_search_messages = {
    "MSG_001" : {"status" : 200, "message" : "Successfully extracted the user details"},
    "MSG_002" : {"status" : 300, "message" : "Could not extract the user details"}
}

# User deletion messages

user_deletion_messages = {
    "MSG_001" : {"status" : 200, "message" : "User deleted successfully"},
    "MSG_002" : {"status" : 300, "message" : "User not found"}
}


# Device registration messages

device_registration_messages = {
    "MSG_001" : {"status" : 200, "message" : "Device successfully registered"},
    "MSG_002" : {"status" : 300, "message" : "Device could not be registered"}
}


# Device listing messages

device_listing_messages = {
    "MSG_001" : {"status" : 200, "message" : "Successfully fetched the device list"},
    "MSG_002" : {"status" : 300, "message" : "No device found"}
}


# Device search messages

device_search_messages = {
    "MSG_001" : {"status" : 200, "message" : "Successfully fetched the device data"},
    "MSG_002" : {"status" : 300, "message" : "No device found"}
}


# Device updation messages

device_update_messages = {
    "MSG_001" : {"status" : 200, "message" : "Device data updated successfully"},
    "MSG_002" : {"status" : 300, "message" : "Could not update device data"}
}


# Device deletion messages

device_deletion_messages = {
    "MSG_001" : {"status" : 200, "message" : "Device deleted successfully"},
    "MSG_002" : {"status" : 300, "message" : "Device not found"},
    "MSG_003" : {"status" : 300, "message" : "Device could not be deleted due to active state"},
    "MSG_004" : {"status" : 300, "message" : "User not found"}
}


# Device status check

device_status_messages = {
    "MSG_001" : {"status" : 200, "message" : "Device status fetched successfully"},
    "MSG_002" : {"status" : 300, "message" : "Device status not found"},
}

# Device status updation

device_status_updation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Device status updated successfully"},
    "MSG_002" : {"status" : 300, "message" : "Device status could not be updated"},
}

# Role creation messages

role_creation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Role created successfully"},
    "MSG_002" : {"status" : 300, "message" : "Could not create the role"}
}


# Role updation messages

role_updation_messages = {
    "MSG_001": {"status": 200, "message": "Successfully updated the role"},
    "MSG_002": {"status": 300, "message": "Could not update the role"},
    "MSG_003": {"status" : 300, "message" : "No device found"}
}


# Role search messages

role_search_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the role"},
    "MSG_002": {"status": 300, "message": "Could not fetch the role"}
}


# Role listing messages

role_listing_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the roles"},
    "MSG_002": {"status": 300, "message": "Could not fetch the roles"}
}


# Rule creation messages

rule_creation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Rule created successfully"},
    "MSG_002" : {"status" : 300, "message" : "Could not create the rule"}
}


# Rule updation messages

rule_updation_messages = {
    "MSG_001": {"status": 200, "message": "Successfully updated the rule"},
    "MSG_002": {"status": 300, "message": "Could not update the rule"},
    "MSG_003": {"status" : 300, "message" : "No device found"}
}


# Rule search messages

rule_search_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the rule"},
    "MSG_002": {"status": 300, "message": "Could not fetch the rule"}
}


# Rule listing messages

rule_listing_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the rules"},
    "MSG_002": {"status": 300, "message": "Could not fetch the rules"}
}


# Action creation messages

action_creation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Action created successfully"},
    "MSG_002" : {"status" : 300, "message" : "Could not create the action"}
}


# Action updation messages

action_updation_messages = {
    "MSG_001": {"status": 200, "message": "Successfully updated the action"},
    "MSG_002": {"status": 300, "message": "Could not update the action"},
    "MSG_003": {"status" : 300, "message" : "No device found"}
}


# Action search messages

action_search_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the action"},
    "MSG_002": {"status": 300, "message": "Could not fetch the action"}
}


# Action listing messages

action_listing_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the actions"},
    "MSG_002": {"status": 300, "message": "Could not fetch the actions"}
}


# Sensor creation messages

sensor_creation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Sensor created successfully"},
    "MSG_002" : {"status" : 300, "message" : "Could not create the sensor"}
}


# Sensor updation messages

sensor_updation_messages = {
    "MSG_001": {"status": 200, "message": "Successfully updated the sensor"},
    "MSG_002": {"status": 300, "message": "Could not update the sensor"},
    "MSG_003": {"status" : 300, "message" : "No device found"}
}


# Sensor search messages

sensor_search_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the sensor"},
    "MSG_002": {"status": 300, "message": "Could not fetch the sensor"}
}


# Sensor listing messages

sensor_listing_messages = {
    "MSG_001": {"status": 200, "message": "Successfully fetched the sensors"},
    "MSG_002": {"status": 300, "message": "Could not fetch the sensors"}
}


# Sensor status check

sensor_status_messages = {
    "MSG_001" : {"status" : 200, "message" : "Sensor status fetched successfully"},
    "MSG_002" : {"status" : 300, "message" : "Sensor status not found"},
}

# Sensor status updation

sensor_status_updation_messages = {
    "MSG_001" : {"status" : 200, "message" : "Sensor status updated successfully"},
    "MSG_002" : {"status" : 300, "message" : "Sensor status could not be updated"},
}

# SSH file retrieval messages

ssh_file_retrieval_messages = {
    "MSG_001": {"status": 200, "message" : "Successfully downloaded the ssh config file"},
    "MSG_002": {"status": 300, "message" : "Could not download the ssh config file"}
}
