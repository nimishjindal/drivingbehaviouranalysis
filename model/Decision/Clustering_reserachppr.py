import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np
from sklearn.cluster import KMeans
from sklearn import preprocessing
import pandas as pd
import time
import os, sys

def rczt(cst,zst,cstMax,zstMax):
  return float((cst/cstMax)/(zst/zstMax))

def rjzt(jq_t,zs_t,jq_t_max,zs_t_max):
  if zs_t != 0: #division by zero
    return float((jq_t/jq_t_max)/(zs_t/zs_t_max))
  return 0

def cluster():
    try:

      columns  = ["Engine_speed","Vehicle_speed","Absolute_throttle_position","Calculated_LOAD_value","Gear_Selection","Class"]

      n = 2

      diff = 6

      df = pd.read_csv("../../data/OBD_main.csv",usecols = columns ,nrows = 10000)

      print("csv extracted")



      df = df.loc[df['Class'] == "A"]
      df = df.loc[df['Gear_Selection'] == 5]


      df.drop(['Class',"Gear_Selection"],1)
      print(df.head(),df.shape[0])


      Engine_speed = df['Engine_speed'].values
      Engine_speed_max = max(Engine_speed)

      Vehicle_speed = df['Vehicle_speed'].values
      Vehicle_speed_max = max(Vehicle_speed)

      Absolute_throttle_position = df['Absolute_throttle_position'].values
      Absolute_throttle_position_max = max(Absolute_throttle_position)

      Calculated_LOAD_value = df['Calculated_LOAD_value'].values
      Calculated_LOAD_value_max = max(Calculated_LOAD_value)

      jq = [(Absolute_throttle_position[i] - Absolute_throttle_position[i-diff]) for i in range(diff,df.shape[0])]
      print("jq created, max is ",max(jq))
      print("jq created, min is ",min(jq))


      zs = [(Engine_speed[i] - Engine_speed[i-diff]) for i in range(diff,df.shape[0])]
      print("zs created, max is ",max(zs))
      print("zs created, min is ",min(zs))


      jq_max = max(jq)
      zs_max = max(zs)
      
      times = [t for t in range(df.shape[0])]

      rcztList = []
      rjztList =  []

      for t in range(df.shape[0]):
        cst = Vehicle_speed[t]
        zst = Engine_speed[t]
        rcztList += [rczt(cst,zst,Vehicle_speed_max,Engine_speed_max)]

      for t in range(len(zs)):
        jq_t = jq[t]
        zs_t = zs[t]
        rjztList += [rjzt(jq_t,zs_t,jq_max,zs_max,)]

      for _ in range(diff):
          rjztList += [0]
            
      data = [('rcztList',rcztList),('rjztList',rjztList),('Calculated_LOAD_value',Calculated_LOAD_value)]
      df = pd.DataFrame.from_items(data)

      colors = ["r.","b.","c.","y."]

      rpm = Engine_speed

      X = np.array(df)
      X = preprocessing.scale(X)

      clf = KMeans(n_clusters=n)
      clf.fit(X)

      print("trained")

      prediction_list = []
      for i in range(len(X)):
          predict_me = np.array(X[i].astype(float))
          predict_me = predict_me.reshape(-1, len(predict_me))
          prediction = clf.predict(predict_me)
          plt.plot(times[i], Engine_speed[i], colors[prediction[0]] , markersize = 3)

      print("making graph")
      plt.show()

    except Exception as e:
      print(e)

cluster()
