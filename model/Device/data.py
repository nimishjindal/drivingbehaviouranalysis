"""

This file tries to implement the functionalities related to the device data in the database.
The file consists of the Action class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) pushdata - Pushes data from the sensor to the bucket
    3) getdata - Gets the sensor data from the bucket

"""

import json
import boto3
import pandas as pd
import datetime
import time
import json
import operator
from bson import json_util
from bson.objectid import ObjectId
from config import constants as const
from model.basemodel import Basemodel
from config.messages  import csv_messages as msg
from config.messages  import put_data_messages as put_msg
from config.messages  import fetch_messages
from config.constants import cols
from common.utils import get_all_columns

client = boto3.client('s3')


class Data(Basemodel):
    def __init__(self, user_id, device_id, sensor_id):

        """

        This method acts as a constructor

        """
        Basemodel.__init__(self)
        self.user_id = user_id
        self.device_id = device_id
        self.sensor_id = sensor_id
        self.key = str(self.user_id) + "_" + str(self.device_id) + "_" + str(self.sensor_id)

        self.offset = 0

    def get_collection_name(self):
        
        key_db = self.db.key_id.find({"key": self.key})

        if key_db.count() == 0:
            k_id = self.db.key_id.insert({"key" : self.key})
            
        else:
            try:
                k_id = key_db[0].get('_id') 
            except Exception as e:
                return 0

        return str(k_id)

    def putdata(self,  length):

        """

        :param user_id:
        :param device_id:
        :param sensor_id:
        :param length:
        :return:

        """
        dataCount = 0
        print ("In putdata method")

        key = self.key 

        try:
            obd_data = self.get_collection_name()

            items = self.db[obd_data].find()
            dataCount = items.count()

            if dataCount > 0:
                df1 = pd.read_csv(const.csv_path_1, nrows = length ,skiprows=range(1, dataCount+1))
                print("csv read, db has some data")
            else:
                df1 = pd.read_csv(const.csv_path_1, nrows = length)
                print("csv read, db has NO data")

            num_rows_in_csv=df1.shape[0]    #number of rows in csv, not in mongoDB

            if num_rows_in_csv == 0:
                message = put_msg['MSG_002']
                message["done"] = dataCount
                return message

            columns = df1.columns.values.tolist()

            for i in range(0, num_rows_in_csv):
                d = {}
                d["timestamp"] = time.time()
            
                for col in columns:
                    try:
                        d[str(col)] = int(df1.iloc[i][col])
                    except:
                        d[str(col)] = str(df1.iloc[i][col])
                try:
                    self.db[obd_data].insert(d)
                except Exception as e:
                    print(e)
            dataCount += 1
  
            message = put_msg['MSG_001']
            message["done"] = dataCount
            return message
        
        except Exception as e:
            message = put_msg['MSG_error']
            message['error'] = str(e)
            print(e)
            return message

    def fetch_data(self, period,length):

        """

        :param user_id:
        :param device_id:
        :param sensor_id:
        :return:

        """

        print ("In fetch_data method")

        length = int(length)
        key = self.key

        data_obd = []

        obd_data = self.get_collection_name()

        try:
            if length > 0:
                data = self.db[obd_data].find().limit(length)
            else:
                data = self.db[obd_data].find()
        
        except Exception as e:
            message =  fetch_messages["MSG_error"]
            message["error"] = e
            print("some exception",e)
            return message

        cols= get_all_columns()

        if data.count() >= 1:
            items_obd = data
            
            for record in items_obd:
               
                d = {}

                for col in cols+["timestamp"]:
                    d[col] = record[col]

                if (period == "default" or period == "now"):
                    data_obd.append(d)
                elif period == "hourly":
                    if time.time() - d["timestamp"] <= 3600:
                        data_obd.append(d)     
                elif period == "daily":
                    if time.time() - d["timestamp"] <= 86400:
                        data_obd.append(d)
                elif period == "week":
                    if time.time() - d["timestamp"] <= 604800:
                        data_obd.append(d)
                elif period == "month":
                    if time.time() - d["timestamp"] <= 2678400:
                        data_obd.append(d)
                else:
                        msg = "invalid period"
                        print(msg)
                        return {"status":300, "message":msg, "length":0}
                        break
               
            message =  fetch_messages["MSG_001"]

            message["data_obd"] = data_obd
            message["data_obd"].sort(key=operator.itemgetter('timestamp'), reverse=True)
            message["length"] = data.count() if length<=0 else length

            return message

        else:
            return fetch_messages["MSG_002"]

    def load(self,start,period,cols):

        try:

            df = pd.read_csv(const.csv_path_1, nrows=period, skiprows=range(1,start), header=0 , usecols = cols)
            df.fillna(0, inplace=True)
            
            message = msg["MSG_001"]
            mydict = df.to_dict(orient="records")
            
            message["data_obd"] = mydict

            return message

        except Exception as e:
            
            message = msg["MSG_002"]
            message["error"] = str(e)
            return message
   
    def CSV_length(self):

        try:
            dataCount = 0   #data already saved in mongodb
            
            obd_data = self.get_collection_name()
            try:
                dataCount = self.db[obd_data].count()
                print("data found some value")
            except Exception as e:
                dataCount = 0
                print("exceptions",e)
                

            df = pd.read_csv(const.csv_path_1)
            count = df.shape[0]
        
            message = {}
            message["status"] = 200
            message["message"] = "fetched total number of records in csv and mongo"
            message["count"] = count
            message["done"] = dataCount
    
            return message

        except Exception as e:

            return {'error':e}