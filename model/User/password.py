"""
This file implements the forgot password and reset password functionality

The file consists of the Password class which has the following methods :-
    1) __init__ - Works as a constructor to initialise the variables/parameters
    2) send_reset_mail - Sends a mail to the user with the reset password link
    3) reset_password - Implements checks and functionalites for resetting the password

"""


from email.mime.text import MIMEText
from bson.objectid import ObjectId
import datetime
from passlib.hash import pbkdf2_sha256
#from common.mail import send_mail
from config.messages import password_messages
from config import constants as const
from model.basemodel import Basemodel


class Password(Basemodel):

    def __init__(self):

        """
        This method initialises the variables/parameters

        """
        Basemodel.__init__(self)
        self.email = None
        self.id = None
        self.new_password = None
        self.confirm_password = None

    def send_reset_mail(self, email):

        """

        :param email:

        This methods consists of the following steps :-
            1) Checks if the user exists in the database. If yes, move to step 2, else, return/exit.
            2) Sends a mail to the user with a link to reset the password (having the unique user ID)

        """

        print( "In send_reset_mail block")

        self.email = email
        if self.db.aceuser.find({"email" : self.email}).count() >= 1:
            receiver = email
            itm = self.db.aceuser.find_one({"email": self.email})
            id = itm.get('_id')
            msg = 'Please visit this <a href='+const.common['UI_BASE_PATH']+'/#/reset/' + str(id) + ">link</a>"
            print(msg)
            msg = MIMEText(msg, 'html')
            send_mail(self.email, msg)
            message = password_messages["MSG_001"]
        else:
            message = password_messages["MSG_002"]

        return message

    def reset_password(self, id, new_password, confirm_password):

        """

        :param id:
        :param new_password:
        :param confirm_password:

        This method consists of the following steps :-
            1) Check if the new password and confirm password match. If they do, move to step 2, else , return/exit.
            2) Updates the user record in the database.

        """

        print( "In reset_password block")

        self.id = id
        self.new_password = new_password
        self.confirm_password = confirm_password

        if self.new_password != self.confirm_password:
            message = password_messages["MSG_003"]
        else:
            if self.db.aceuser.update_one({'_id': ObjectId(id)},{'$set': {'password': pbkdf2_sha256.hash(self.new_password), 'updatedon' : datetime.datetime.utcnow()}}):
                message = password_messages["MSG_004"]
            else:
                message = password_messages["MSG_005"]

        return message


