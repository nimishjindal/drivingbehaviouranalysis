"""

This file is used for implementing the sensor registration portion of the website.
The file consists of the Registration_S class which implements the following methods :-
    1) __init__ - Initialises the variables/parameters
    2) register_sensor - Tries to insert data regarding the sensor into the device database
        a)  If successful -> Return success message
        b)  If failure -> Return failure message

"""

import boto3
from bson.objectid import ObjectId
from model.basemodel import Basemodel
from config.messages  import sensor_creation_messages, sensor_listing_messages, sensor_updation_messages, \
    sensor_search_messages
from config import constants as const



class Sensor(Basemodel):
        def __init__(self):

            """

            This method acts as a constructor

            """

            Basemodel.__init__(self)
            self.user_id = None
            self.device_id = None
            self.sensor_code = None
            self.sensor_name = None
            self.sensor_type = None
            self.sensor_gpio = None
            self.comm_protocol = None
            self.timestamp = None
            self.state = None
            self.id = None

        def create_sensor(self, user_id, device_id, sensor_code, sensor_name, sensor_type, sensor_gpio, comm_protocol,
                            timestamp):

            """

            This method registers a new sensor for the device

            :param user_id:
            :param device_id:
            :param sensor_code:
            :param sensor_name:
            :param sensor_type:
            :param sensor_gpio:
            :param comm_protocol:
            :param timestamp:
            :return:
            """

            print ("In register_sensor block")

            self.user_id = user_id
            self.device_id = device_id
            self.sensor_code =sensor_code
            self.sensor_name = sensor_name
            self.sensor_type = sensor_type
            self.sensor_gpio = sensor_gpio
            self.comm_protocol = comm_protocol
            self.timestamp = timestamp
            self.state = 'OK'
            self.id = ObjectId()

            sensor_data = {
                "code" : self.sensor_code,
                "name" : self.sensor_name,
                "type" : self.sensor_type,
                "gpio" : self.sensor_gpio,
                "comm_protocol" : self.comm_protocol,
                "timestamp" : self.timestamp,
                "state" : self.state,
                "id" : self.id
            }

            try:
                self.db.acedevices.update(
                    {'$and': [{"_id": ObjectId(self.device_id)}, {"userId": ObjectId(self.user_id)}]},
                    {'$push': {'sensors': sensor_data}})

                sensor_code = sensor_code.replace(" ", "_")

                # table_name = str(user_id) + "_" + str(device_id) + "_" + str(sensor_code)
                #
                # dynamodb = boto3.resource('dynamodb')
                #
                #
                # table = dynamodb.create_table(
                #     TableName=table_name,
                #     KeySchema=[
                #         {
                #             'AttributeName': 'timestamp',
                #             'KeyType': 'HASH'
                #         }
                #     ],
                #     AttributeDefinitions=[
                #         {
                #             'AttributeName': 'timestamp',
                #             'AttributeType': 'S'
                #         }
                #
                #     ],
                #     ProvisionedThroughput={
                #         'ReadCapacityUnits': 5,
                #         'WriteCapacityUnits': 5
                #     }
                # )

                message = sensor_creation_messages["MSG_001"]
            except Exception as e:
                print (e)
                message = sensor_creation_messages["MSG_002"]

            return message

        def update_sensor(self, device_id, sensor_code, sensor_id, sensor_name, sensor_type, sensor_gpio, comm_protocol):

            print ("In update_sensor block")

            self.device_id = device_id
            self.sensor_code = sensor_code
            self.sensor_name = sensor_name
            self.sensor_type = sensor_type
            self.sensor_gpio = sensor_gpio
            self.comm_protocol = comm_protocol
            self.id = sensor_id

            if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:

                if self.db.acedevices.update({
                    'sensor.id': self.id
                }, {'$set': {'sensors.$.name': self.sensor_name, 'sensors.$.type': self.sensor_type,
                             'sensors.$.gpio': self.sensor_gpio, 'sensors.$.comm_protocol': self.comm_protocol,}}):
                    message = sensor_updation_messages["MSG_001"]
                else:
                    message = sensor_updation_messages["MSG_002"]
            else:
                message = sensor_updation_messages["MSG_003"]

            return message

        def search_sensor(self, device_id, sensor_code):

            """
            This method searches for a particular sensor registered for a device using the sensor code

            :param device_id:
            :param sensor_code:
            :return:
            """

            print ("In search_sensor block")

            self.device_id = device_id
            self.id = sensor_code
            if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
                item = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
                message = sensor_search_messages["MSG_001"]
                message["sensor"] = []
                for record in item:
                    for sensor in record["sensors"]:
                        if sensor["id"] == self.id:
                            s = {}
                            s["code"] = str(sensor["code"])
                            s["name"] = str(sensor["name"])
                            s["type"] = str(sensor["type"])
                            s["gpio"] = str(sensor["gpio"])
                            s["comm_protocol"] = str(sensor["comm_protocol"])
                            message["sensor"].append(s)
            else:
                message = sensor_listing_messages["MSG_002"]

            return message

        def list_sensors(self, device_id):

            """

            This method lists the sensors for a particular device and returns the corresponding message

            :param device_id:
            :return:

            """

            print ("In list_sensors block")

            self.device_id = device_id
            if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
                items = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
                message = sensor_listing_messages["MSG_001"]
                message["sensors"] = []
                for record in items:
                    for sensor in record["sensors"]:
                        s = {}
                        s["code"] = str(sensor["code"])
                        s["name"] = str(sensor["name"])
                        s["type"] = str(sensor["type"])
                        s["gpio"] = str(sensor["gpio"])
                        s["comm_protocol"] = str(sensor["comm_protocol"])
                        s["id"] = str(sensor["id"])
                        message["sensors"].append(s)
            else:
                message = sensor_listing_messages["MSG_002"]

            return message



