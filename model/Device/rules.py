"""

This file tries to implement the functionalities related to the device rules in the database.
The file consists of the Rule class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) create_rule - Creates the rule for a particular device
    3) update_rule - Updates the rule for a particular device
    4) list_rules - Lists the rule for a particular device

"""

import json
from bson import json_util
from bson.objectid import ObjectId
from model.basemodel import Basemodel
from config.messages  import rule_creation_messages, rule_listing_messages, rule_updation_messages, rule_search_messages


class Rule(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.user_id = None
        self.device_id = None
        self.rule_code = None
        self.rule_name = None
        self.rule_value = None
        self.rule_attribute = None
        self.rule_status = None
        self.rule_desc = None
        self.rule_param = None
        self.rule_actions = None
        self.rule_condition = None
        self.rule_desc = None
        self.rule_type = None
        self.rule_msg = None

    def create_rule(self, user_id, device_id, rule_code, rule_name, rule_value, rule_attribute, rule_status, rule_actions, rule_condition, rule_desc, rule_param, rule_type, rule_msg) :

        """

        This method tries to create a rule for a device in the database and returns the corresponding message

        :param user_id:
        :param device_id:
        :param rule_code:
        :param rule_name:
        :param rule_value:
        :param rule_attribute:
        :param rule_status:
        :param rule_desc:
        :return:

        """

        print ("In create_rule block")

        self.user_id = user_id
        self.device_id = device_id
        self.rule_name = rule_name
        self.rule_code = rule_code
        self.rule_value = rule_value
        self.rule_attribute = rule_attribute
        self.rule_status = rule_status
        self.rule_desc = rule_desc
        self.rule_condition = rule_condition
        self.rule_param = rule_param
        self.rule_actions = rule_actions
        self.rule_type = rule_type
        self.rule_msg = rule_msg

        rule_data = {
                    "code" : self.rule_code, 
                    "name" : self.rule_name, 
                    "value" : self.rule_value,
                    "attrib" : self.rule_attribute, 
                    "status" : self.rule_status,
                    "desc" : self.rule_desc, 
                    "act": self.rule_actions, 
                    "condition" : self.rule_condition, 
                    "param" : self.rule_param,
                    "type" : self.rule_type,
                    "msg" : self.rule_msg
                    }
        print (rule_data)
        try :
            self.db.acedevices.update({'$and': [{"_id" : ObjectId(self.device_id)}, {"userId" : ObjectId(self.user_id)}]},
                                      {'$push': {'rules': rule_data}})
            message =  rule_creation_messages["MSG_001"]
        except Exception as e:
            print (e)
            message = rule_creation_messages["MSG_002"]

        return message

    def update_rule(self, device_id, rule_code, rule_name, rule_value, rule_attribute, rule_status,rule_actions, rule_condition, rule_desc,rule_param, rule_type, rule_msg):

        """

        This method updates the rule for a device and returns the corresponding message

        :param device_id:
        :param rule_code:
        :param new_rule_name:
        :param new_rule_value:
        :param new_rule_status:
        :param new_rule_attribute:
        :param new_rule_desc:
        :return:

        """

        print ("In update_rule block")

        self.device_id = device_id
        self.rule_name = rule_name
        self.rule_code = rule_code
        self.rule_value = rule_value
        self.rule_attribute = rule_attribute
        self.rule_status = rule_status
        self.rule_desc = rule_desc
        self.rule_condition = rule_condition
        self.rule_param = rule_param
        self.rule_actions = rule_actions
        self.rule_type = rule_type
        self.rule_msg = rule_msg

        new_rule_data = { 
                    "name" : self.rule_name, 
                    "value" : self.rule_value,
                    "attrib" : self.rule_attribute, 
                    "status" : self.rule_status,
                    "desc" : self.rule_desc, 
                    "act": self.rule_actions, 
                    "condition" : self.rule_condition, 
                    "param" : self.rule_param,
                    "type" : self.rule_type,
                    "msg" : self.rule_msg
                    }

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:

            if self.db.acedevices.update({
                'rules.code': self.rule_code
                },{'$set' : {
                            'rules.$.name':self.rule_name, 'rules.$.value':self.rule_value,
                            'rules.$.desc':self.rule_desc, 'rules.$.status' : self.rule_status,
                            'rules.$.attrib':self.rule_attribute,'rules.$.param':self.rule_param,
                            'rules.$.condition':self.rule_condition,'rules.$.act':self.rule_actions,
                            'rules.$.type':self.rule_type,'rules.$.msg':self.rule_msg
                        }
                }):
                message = rule_updation_messages["MSG_001"]
            else:
                message = rule_updation_messages["MSG_002"]
        else:
            message = rule_updation_messages["MSG_003"]

        return message

    def search_rule(self, device_id, rule_code):

        self.device_id = device_id
        self.rule_code = rule_code
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            item = self.db.acedevices.find({"_id": ObjectId(self.device_id)})
            message = rule_search_messages["MSG_001"]
            message["rule"] = []
            for record in item:
                for rule in record["rules"]:
                    if rule["code"] == self.rule_code:
                        ru = {}
                        ru["code"] = str(rule["code"])
                        ru["name"] = str(rule["name"])
                        ru["type"] = str(rule["type"])
                        ru["desc"] = str(rule["desc"])
                        ru["val"] = str(rule["value"])
                        ru["value"] = str(rule["value"])
                        ru["status"] = str(rule["status"])
                        ru["enabled"] = str(rule["status"])
                        ru["attrib"] = str(rule["attrib"])
                        ru["param"] = str(rule["param"])
                        ru["condition"] = str(rule["condition"])
                        ru["act"] = str(rule["act"])
                        message["rule"].append(ru)
        else:
            message = rule_search_messages["MSG_002"]

        return message

    def list_rules(self, device_id):

        """

        This method lists the rules for a particular device and returns the corresponding message

        :param device_id:
        :return:

        """

        print ("In list_rules block")

        self.device_id = device_id
        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:
            items = self.db.acedevices.find({"_id" : ObjectId(self.device_id)})
            message = rule_listing_messages["MSG_001"]
            message["rules"] = []
            for record in items:
                for rule in record["rules"]:
                    ru = {}
                    ru["code"] = str(rule["code"])
                    ru["name"] = str(rule["name"])
                    ru["type"] = str(rule["type"])
                    ru["desc"] = str(rule["desc"])
                    ru["value"] = str(rule["value"])
                    ru["status"] = str(rule["status"])
                    ru["attrib"] = str(rule["attrib"])
                    ru["param"] = str(rule["param"])
                    ru["condition"] = str(rule["condition"])
                    ru["act"] = str(rule["act"])
                    ru["msg"] = str(rule["msg"])
                    message["rules"].append(ru)
        else :
            message = rule_listing_messages["MSG_002"]

        return message