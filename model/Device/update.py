"""

This file tries to implement the device updation functionality of the website.
The file consists of the U_Device class which contains the following methods :-
    1) __init__ - Initialises the variables/parameters
    2)  update - Implements the device updation functionality

"""


import json
from model.basemodel import Basemodel
from config.messages  import device_update_messages
from bson.objectid import ObjectId
from bson import json_util
import datetime


class U_Device(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.device_id = None
        self.device = None
        self.devicetype = None
        self.devicecode = None
        self.sshuser = None
        self.password = None
        self.ipaddress = None
        self.port = None
        self.macid = None
        self.model = None
        self.networkid = None
        self.enabled = None
        self.accesspoint = None
        self.datapoint = None
        self.dataendpoints = None
        self.updatedon = None
        self.longitude = None
        self.latitude = None
        self.network_type = None

    def update(self, device_id, device, devicetype, devicecode, sshuser, password, ipaddress, port, macid, model,
               networkid, enabled, accesspoint, datapoint, dataendpoints, updatedon, longitude, latitude, network_type):

        """

        This method implements the following functionality :-
            1)  Tries to update the device details in the database
            2)  Returns the corresponding message

        :param device_id:
        :param device_name:
        :return:

        """

        print ("In update device block")

        self.device_id = device_id
        self.device = device
        self.devicetype = devicetype
        self.devicecode = devicecode
        self.sshuser = sshuser
        self.password = password
        self.ipaddress = ipaddress
        self.port = port
        self.macid = macid
        self.model = model
        self.networkid = networkid
        self.enabled = enabled
        self.accesspoint = accesspoint
        self.datapoint = datapoint
        self.dataendpoints = dataendpoints
        self.updatedon = updatedon
        self.longitude = longitude
        self.latitude = latitude
        self.network_type = network_type

        # print "In device updation block"

        if self.db.acedevices.update_one({'_id': ObjectId(self.device_id)},
                                         {'$set': {'device': self.device,
                                                   'devicetype' : self.devicetype,
                                                   'devicecode' : self.devicecode,
                                                   'sshuser' : self.sshuser,
                                                   'password' : self.password,
                                                   'ipaddress' : self.ipaddress,
                                                   'port' : self.port,
                                                   'macid' : self.macid,
                                                   'model' : self.model,
                                                   'networkid' : self.networkid,
                                                   'enabled' : self.enabled,
                                                   'accesspoint' : self.accesspoint,
                                                   'datapoint' : self.datapoint,
                                                   'dataendpoints' : self.dataendpoints,
                                                   'updatedon' : self.updatedon,
                                                   'latitude' : self.latitude,
                                                   'longitude' : self.longitude,
                                                   'network_type' : self.network_type}}):
            message = device_update_messages["MSG_001"]
        else:
            message = device_update_messages["MSG_002"]
        return message