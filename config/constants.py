"""
Stores the Configuration constants
"""

database = dict(
    CLIENT = None,
    DBHOST_SERVER = "127.0.0.1:5000",
    #DBHOST_LOCAL ="ds151180.mlab.com:51180",		
    DBHOST_LOCAL = "localhost:27017",	
    #DBHOST_LOCAL = "localhost:27018",		
	
    DBNAME = "acedb",
    #DBUSER =  "sureenawadhwa",
    DBUSER =  "nttadmin",
    #DBPASS = "rekha104"		
    DBPASS = "password"			
)

common = dict(
    UI_BASE_PATH = "http://localhost:5100",
    SECRET_KEY = "ACE_AUTH",
    LOG_FILE_PATH = "/tmp/app.log",
    APP_MODE = "LOCAL"  # It can be LOCAL or PROD
)

appaddress = "192.168.1.100"

csv_path_1 = "data/OBD_main.csv"
csv_path_2 = "data/april-2017.csv"

obdcsv = "data/OBD.csv"
obd_fields = "config/fields_all.txt"
old_obd = "config/fields.txt"
n_file = "config/n.txt"

# csv_path_temp = "/projects/Ace_Core/src/Ace/TEMP_main.csv"
#
# csv_path = "data/OBD_main.csv"
# csv_path_temp = "data/TEMP_main.csv"

#cols = ["Absolute_throttle_position","Accelerator_Pedal_value",  "Class","Engine_speed","Fuel_consumption","Intake_air_pressure","Vehicle_speed"]
cols = ["Engine_speed","Vehicle_speed","Absolute_throttle_position","Calculated_LOAD_value"]
