from flask import Blueprint, request, jsonify, session, render_template
from flask import current_app as app
from random import randint
from flask_ask import Ask, statement, question, session
from bson import ObjectId
import requests
import json
import urllib
from db.connect import Database

from model.Device.list import Device


lexservice = Blueprint('lexservice', __name__, url_prefix="/obd_data")
ask = Ask(blueprint=lexservice)
database = Database()
db = database.initialise_client()


def get_hospitals(lat,long):
    url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?location=" + str(lat) + "," + str(
        long) + "&radius=5000&keyword=hospital%%&near%me&key=AIzaSyA9Nz08jsVYOhOuPG3pYS5YpJ2X4fCuU8U"
    r = json.loads(requests.post(url).text)

    places_ids = []
    places_names = []

    for i in range(0, len(r["results"])):
        places_ids.append(r["results"][i]["place_id"])

    for i in range(0, 3):
        u = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + str(
            places_ids[i]) + "&key=AIzaSyA9Nz08jsVYOhOuPG3pYS5YpJ2X4fCuU8U"
        re = json.loads(requests.post(u).text)
        places_names.append(re["result"]["name"])

    print (places_names)

    return places_names


def get_service_centres(lat,long):
    url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?location=" + str(lat) + "," + str(
        long) + "&radius=5000&keyword=car%service%center&near%me&key=AIzaSyA9Nz08jsVYOhOuPG3pYS5YpJ2X4fCuU8U"
    r = json.loads(requests.post(url).text)

    places_ids = []
    places_names = []

    for i in range(0,len(r["results"])):
        places_ids.append(r["results"][i]["place_id"])

    for i in range(0, 3):
        u = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + str(
            places_ids[i]) + "&key=AIzaSyA9Nz08jsVYOhOuPG3pYS5YpJ2X4fCuU8U"
        re = json.loads(requests.post(u).text)
        places_names.append(re["result"]["name"])

    print (places_names)

    return places_names


def get_location_details(lat,lon):

    header = {"user-key": "e5643d5519ea87a914cf5d44e806fa61"}
    Zomato_request_url = "https://developers.zomato.com/api/v2.1/geocode?lat=" + lat + "&lon=" + lon
    r = requests.post(Zomato_request_url, headers=header)
    res = json.loads(r.text)
    r.close()

    return res


def fetch_data():
    data = []
    if db.obd_data.find({"key" : "5a68dde1fca66f53c6403e89_5acc93a22a92c35117bde7b9_5acc93d82a92c35117bde7c4"}).count() >= 1:
        items = db.obd_data.find({"key": "5a68dde1fca66f53c6403e89_5acc93a22a92c35117bde7b9_5acc93d82a92c35117bde7c4"})
        for record in items:
            for d in record["data"]:
                for d in record["data"]:
                    data.append(d)
    return data[-1]


@ask.launch  # On launching the skill
def start_skill():

    # message = "Welcome to your personal OBD assistant. The current OBD statistics are : "
    # data = fetch_data()
    # throttle = data["absolute_throttle_position"]
    # load = data["load_value"]
    # torque = data["engine_torque"]
    # rpm = data["engine_speed"]
    # coolant_temp = data["engine_coolant_temp"]
    # intake_air_pressure = data["intake_air_pressure"]
    #
    # message += "Throttle" + " . " + str(throttle) + " . "
    # message += "Load" + " . " + str(load) + " . "
    # message += "Torque" + " . " + str(torque) + " . "
    # message += "RPM" + " . " + str(rpm) + " . "
    # message += "Engine coolant temperature" + " . " + str(coolant_temp) + " . "
    # message += "Intake Air Pressure" + " . " + str(intake_air_pressure) + " . "
    #
    # print message

    message = "Welcome to Acela . "

    return question(message)


@ask.intent("ThrottleIntent")
def share_throttle():
    message = "The current throttle is . "
    data = fetch_data()["absolute_throttle_position"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("LoadIntent")
def share_load():
    message = "The current load is . "
    data = fetch_data()["load_value"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("TorqueIntent")
def share_torque():
    message = "The current torque is . "
    data = fetch_data()["engine_torque"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("RPMIntent")
def share_score():
    message = "The current rpm is . "
    data = fetch_data()["engine_speed"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("CoolantIntent")
def share_score():
    message = "The current coolant temperature is . "
    data = fetch_data()["engine_coolant_temp"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("PressureIntent")
def share_score():
    message = "The current intake air pressure is . "
    data = fetch_data()["intake_air_pressure"]
    message += str(data) + " . "
    print (message)
    return question(message)


@ask.intent("GetRestaurants")
def share_restaurants():
    details = get_location_details("28.7514355", "77.11400649999996")
    l = int(len(details["nearby_restaurants"]))
    message = "The following are the top restaurants near you . "
    for i in range(0, l):
        message += "Name . " + str(details["nearby_restaurants"][i]["restaurant"]["name"]) + " . "
        message += "Average cost for 2 . " + str(details["nearby_restaurants"][i]["restaurant"]["average_cost_for_two"]) + " . "
        message += "Average Rating . " + str(details["nearby_restaurants"][i]["restaurant"]["user_rating"]["aggregate_rating"].
                                             replace(".", " point ")) + " . "

    return question(message)


@ask.intent("GetServiceCentres")
def share_service_centres():
    places = get_service_centres("28.7514355", "77.11400649999996")
    message = "The following are some of the vehicle service centres near you : "
    for i in range(0,len(places)):
        message += str(places[i]) + " . "

    return question(message)


@ask.intent("GetHospitals")
def share_hospitals():
    places = get_hospitals("28.7514355", "77.11400649999996")
    message = "The following are the hospitals near you : "
    for i in range(0, len(places)):
        message += str(places[i]) + " . "

    return question(message)


@ask.intent("AMAZON.StopIntent")
def game_over():
    score = session.attributes.get('score', 0)
    game_over_msg = render_template('game_over', score=score)
    return statement(game_over_msg)


@ask.session_ended
def session_ended():
    return "", 200
