import time
import datetime
import urllib
import json
import ssl
from flask import jsonify,session
from flask import current_app as app
from db.connect import Database
from config import constants as const
from bson.objectid import ObjectId
from config.messages  import token_validation_message
from config.messages  import ssh_file_retrieval_messages
#from common import mqtt_connect as mqtt

database = Database()
db = database.initialise_client()
data = [] #HERE!

# Function to fetch the details from the JWT

def get_all_columns():
    with open(const.obd_fields) as f:
      line = f.readlines()
    line = line[0].split(",")
    return line


def check_sess():

    if 'token' in session:
        print("already logged in ")
        return True

    return False

#def check_auth(token):
def check_auth(token="addLater"):

    return {"status": 1 , "email": session["data"]["email"] }

# Function to validate the token for email and token time


def validate_token(email, token):
    print( "In validate_token block")
    database = Database()
    db = database.initialise_client()
    if db.aceuser.find({"email": email}).count() >= 1: # User exists in the database
        itm = db.aceuser.find_one({"email": email})
        db_token = itm.get('token')

        # Extra validation for token expiration

        token_time = itm.get('token_time')
        current_time = time.time()
        time_difference = (float(current_time) - float(token_time))/60  # Difference in minutes
        print( "Time difference in minutes : " + str(time_difference))
        if db_token == token :  # The token matches
            if time_difference <= 60:  # Valid time difference
                user_id = itm.get('_id')
                db.aceuser.update_one({'_id': ObjectId(user_id)},
                                          {'$set': {'token_time': current_time}})
                message = token_validation_message["MSG_001"]
            else:  # Token time has expired
                message = token_validation_message["MSG_002"]
        else:  # Token does not match
            message = token_validation_message["MSG_003"]
    else:  # User does not exist in the database
        message = token_validation_message["MSG_004"]

    return message


# Function to fetch the user_id from the user database by using the email ID


def get_userid(email):
    print("In get_userid block")
    database = Database()
    db = database.initialise_client()
    itm = db.aceuser.find_one({"email": email})
    id = itm.get('_id')
    return id


def get_length(device_id, field):
    print( "In get_length block")
    database = Database()
    db = database.initialise_client()
    if db.acedevices.find({"_id" : ObjectId(device_id)}).count()>=1:
        items = db.acedevices.find_one({"_id" : ObjectId(device_id)})
        for record in items:
            if field == "roles":
                if len(items["roles"]) >= 1:
                    length = len(items["roles"])
                else :
                    length = 0
            elif field == "rules":
                if len(items["rules"]) >= 1:
                    length = len(items["rules"])
                else :
                    length = 0
            elif field == "sensors":
                if len(items["rules"]) >= 1:
                    length = len(items["sensors"])
                else :
                    length = 0
            elif field == "actions":
                if len(items["actions"]) >= 1:
                    length = len(items["actions"])
                else :
                    length = 0

    print( "Length : " + str(length))
    return length


# This file downloads the ssh utility file from the AWS S3 bucket to the user's PC


def download_file(filename):

    url = "https://s3-us-west-2.amazonaws.com/acecoredemo/" + filename
    # if urllib.urlretrieve(url, filename):
    #     message = ssh_file_retrieval_messages["MSG_001"]
    # else:
    #     message = ssh_file_retrieval_messages["MSG_002"]
    return url

"""
# This function is used to publish data to mqtt Topic
def publish(topic,payload):
    # Fetch the parameters
    client = mqtt.connectMqttBroker()
    client.subscribe(topic)
    payloadData = json.dumps(payload)
    client.publish(topic, payloadData)
    result = json.dumps({"Published":"Success", "topic":topic})
    return result
"""