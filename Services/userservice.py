from flask import Blueprint, request, jsonify, session,render_template ,redirect, flash
import json
from flask import current_app as app
import random
from passlib.hash import pbkdf2_sha256
import datetime

from common.utils import validate_token, check_auth, get_userid #, publish
from model.User.login import Login
from model.User.registration import Registration
from model.User.password import Password
from model.User.update_details import Update
from model.User.delete import Delete
from model.User.signout import User
from model.User.profile import Profile
from model.User.profiledetails import ProfileDetails
from config.messages  import registration_messages
from werkzeug.exceptions import default_exceptions

userservice = Blueprint('userservice', __name__, url_prefix='/user')

# Handles the registration request and parameters
@userservice.route("/register", methods=["POST"])
def register():

    # Fetch the parameters
   
    if request.form["password"] != request.form["re-password"]:  
        flash(registration_messages["MSG_004"])
        return jsonify(registration_messages["MSG_004"])

    password = pbkdf2_sha256.hash(request.form["password"])
    first_name = request.form["firstName"]
    last_name = request.form["lastName"]
    email = request.form["email"]

    created_on = datetime.datetime.utcnow()
    updated_on = datetime.datetime.utcnow()
    last_login_on = datetime.datetime.utcnow()

    status = "0" # inactive:active:logged in
    
    username = first_name + " " + last_name

    user = Registration(username, email, password,  created_on, updated_on,
                        last_login_on, status, first_name, last_name)

    message = user.register_user()

    # print message

    # ---------- Logger ----------
    app.logger.info(message["message"])
    try:
        flash(message["message"])
        return jsonify(message)
    except Exception as e:
        app.logger.info(str(e))
        return str(e)

# Route to handle the submitted email id in the forgot password form
@userservice.route("/forgotPassword", methods=["GET", "POST"])
def forgotPassword():
    # ---------- JSON DATA ----------
    email = request.json["email"]
    password = Password()
    message = password.send_reset_mail(email)
    # ---------- Logger ----------
    if message["status"] == 200 :
        app.logger.info(message["message"])
    elif message["status"] == 300:
        app.logger.error(message["message"])
    return jsonify(message)

# Route to handle the new submitted password
@userservice.route("/resetPassword", methods=["GET", "POST"])
def resetPassword():

    # ---------- JSON DATA ----------
    id = request.json["id"]
    new_password = request.json["password"]
    confirm_password = request.json["confirmPassword"]
    password = Password()
    message = password.reset_password(id, new_password, confirm_password)

    if message["status"] == 200 :
        app.logger.info(message["message"])
    elif message["status"] == 300 :
        app.logger.error(message["message"])

    return jsonify(message)

@userservice.route('/login', methods=['GET', 'POST'])
def index():

    email = request.args["email"]
    password = request.args["password"]
    _login = Login()
    message = _login.doLogin(email, password)
    if message["status"] == 200 :   
        message = _login.validateOTP(email) #generating token
        session["token"] = message["token"]
        session["data"] = message["data"]
        return jsonify({ 'status':message['status'], 'message':message['message'] })

    else: #message["status"] == 300 :
        flash('Incorrect username or password.')
        return jsonify(message)

# Route to redirect to the profile page after logging in
@userservice.route("/profile", methods = ['GET','POST'])
def profile(self, user_id):
    self.userId = user_id
    if self.db.aceuserprofile.find({"userId": ObjectId(self.userId)}).count() >= 1:
        items = self.db.aceuserprofile.find({"userId": ObjectId(self.userId)})
        message = profile_messages["MSG_004"]
        for record in items:
            p = {}
            print(record)
            p["firstName"] = str(record["firstName"])
            p["lastName"] = str(record["lastName"])
            p["email"] = str(record["email"])
            p["subscribed"] = record["subscribed"] if 'subscribed' in record else False
            message["profileData"] = p
        else:
            message = profile_messages["MSG_005"]

        return render_template("profile.html")

# Route to handle the profile page after otp
@userservice.route("/otpauth", methods=["GET", "POST"])
def otpauth():

    # ---------- JSON DATA ----------
    email = request.json["email"]
    otp = request.json["otp"]
    _login = Login()
    message = _login.validateOTP(email, otp)
    if message["status"] == 200:
        app.logger.info(message["message"])
        session["data"] = message["data"]
        session["token"] = message["token"]
    elif message["status"] == 300:
        app.logger.info(message["message"])

    return jsonify(message)

# Route to handle the signout functionality
@userservice.route('/signout', methods = ["GET", "POST"])
def signout():
    u = User()
    token = request.headers["Authorization"]
    response = check_auth(token)
    print (response)
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        message = u.signout(user_id)
        return jsonify(message)
    else:
        return jsonify(response)

# Route to update the user details
@userservice.route('/update', methods=["GET", "POST"])
def update():
    #token = request.headers["Authorization"]
    response = check_auth(token)
    #print (response)
    if response["status"] == 1:
        user_id = get_userid(response["email"])
        
        inp = ""
        fld = ""

        for input , field in request.args.items:
            inp = input
            fld = field

        app.logger.info(inp,fld)
        
        user = update(user_id,inp , fld)

        message = user.update_details()
 
        # ---------- Logger ----------
        if message["status"] == 200:
            app.logger.info(message["message"])
        elif message["status"] == 300:
            app.logger.error(message["message"])

        return jsonify(message)
    else:
        return jsonify(response)

# Route to delete a particular user - Preferably by the admin
@userservice.route('/delete_user/<user_id>', methods=["GET", "POST"])
def delete_user(user_id=None):
    d = Delete()
    message = d.delete_user(user_id)
    return jsonify(message)

@userservice.route("/updateprofile", methods=["POST"])
def updateprofile():
    u = User()
    #token = request.headers["Authorization"]
    response = check_auth(token)
    #print (response)
    if response["status"] == 1:
        user_id = get_userid(response["email"])

    if(user_id):
    # Fetch the parameters
        form = request.json["ProfileForm"]["subscribed"]
        created_on = datetime.datetime.utcnow()
        updated_on = datetime.datetime.utcnow()

        userprofile = Profile(first_name, last_name, email, website, company, address, country, state, city, zipcode,
                            details, subscribed, created_on, updated_on)

        message = userprofile.create_profile(user_id)
        # ---------- Logger ----------
        if message["status"] == 200 :
            app.logger.info(message["message"])
        elif message["status"] == 300 :
            app.logger.error(message["message"])
    else:
        app.logger.error(message["User Id not found"])

    return jsonify(message)

@userservice.route("/getprofile", methods=["GET"])
def getprofile():


    u = User()
    #token = request.headers["Authorization"]
    #response = check_auth(token)
    response = {"status" : 1}
    
    if response["status"] == 1:
        user_id = get_userid(session["data"]["email"])
        print("uid"+str(session["data"]["email"]))

    userprofile = ProfileDetails()
    if(user_id):
        message = userprofile.getProfile(user_id)
        # ---------- Logger ----------
        if message["status"] == 200 :
            app.logger.info(message["message"])
        elif message["status"] == 300 :
            app.logger.error(message["message"])
    else:
        app.logger.error(message["User Id not found"])

    return jsonify(message)

    @userservice.route("/dashboard", methods = ["GET", "POST"])
    def dashboard():
        print("dash")
        return "dash" #render_template ("dashboard.html")