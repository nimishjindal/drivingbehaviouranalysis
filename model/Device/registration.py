"""

This file is used for implementing the device registration portion of the website.
The file consists of the Registration_D class which implements the following methods :-
    1) __init__ - Initialises the variables/parameters
    2) register_device - Tries to insert data into the devices database
        a)  If successful -> Return success message
        b)  If failure -> Return failure message

"""

from model.basemodel import Basemodel
from config.messages  import device_registration_messages
from config import constants as const


class Registration_D(Basemodel):
        def __init__(self):

            """

            This method acts as a constructor

            """

            Basemodel.__init__(self)
            self.user_id = None
            self.device = None
            self.model = None
            self.devicecode = None
            self.macid = None
            self.ip = None
            self.port = None
            self.type = None
            self.access_point = None
            self.ssh_user = None
            self.ssh_password = None
            self.networkid = None
            self.created_on = None
            self.updated_on = None
            self.last_login_on = None
            self.status = None
            self.actions = None
            self.datapoint = None
            self.data_endpoints = None
            self.longitude = None
            self.latitude = None
            self.network_type = None
            self.sensors = None
            self.state = None

        def register_device(self, user_id, device_name, device_type, devicecode, device_macid, device_model, ssh_user,
                                    ssh_password, ip_address, port, access_point, network_id, created_on,
                                    updated_on, last_login_on, status, actions, datapoint, data_endpoints,
                                    longitude, latitude, network_type, roles, rules, sensors):

            """

            :param email:
            :param ip_address:
            :param username:
            :param password:
            :param type:
            :return:

            This method fetches the form data and adds the device data to the devices database

            """

            print ("In register_device block")

            self.user_id = user_id
            self.device = device_name
            self.device_type = device_type
            self.devicecode = devicecode
            self.macid = device_macid
            self.device_model = device_model
            self.ssh_user = ssh_user
            self.ssh_password = ssh_password
            self.ip_address = ip_address
            self.port = port
            self.access_point = access_point
            self.network_id = network_id
            self.created_on = created_on
            self.updated_on = updated_on
            self.last_login_on = last_login_on
            self.status = status
            self.actions = actions
            self.datapoint = datapoint
            self.data_endpoints = data_endpoints
            self.longitude = longitude
            self.latitude = latitude
            self.network_type = network_type
            self.roles = roles
            self.rules = rules
            self.sensors = sensors
            self.state = "0"


            try:
                self.db.acedevices.insert({
                    "userId" : self.user_id,
                    "device" : self.device,
                    "devicetype" : self.device_type,
                    "devicecode" : self.devicecode,
                    "macid" : self.macid,
                    "model" : self.device_model,
                    "sshuser": self.ssh_user,
                    "password": self.ssh_password,
                    "ipaddress" : self.ip_address,
                    "port" : self.port,
                    "accesspoint" : self.access_point,
                    "networkid" : self.network_id,
                    "created_on" : self.created_on,
                    "updatedon" : self.updated_on,
                    "last_login_on" : self.last_login_on,
                    "enabled" : self.status,
                    "actions" : self.actions,
                    "datapoint" : self.datapoint,
                    "dataendpoints" : self.data_endpoints,
                    "longitude" : self.longitude,
                    "latitude" : self.latitude,
                    "network_type" : self.network_type,
                    "roles" : self.roles,
                    "rules" : self.rules,
                    "sensors" : self.sensors,
                    "state" : self.state
                })

                message = device_registration_messages["MSG_001"]
                return message
            except Exception as e:
                print (e)
                message = device_registration_messages["MSG_002"]
                return message
