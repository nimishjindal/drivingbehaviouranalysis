from flask import Blueprint, request, jsonify, session
from flask import current_app as app
import random
import datetime
from model.Decision.Clustering_copy import Clustering
from model.Device.columns import Columns
from common.utils import validate_token, check_auth, get_userid #, publish

ml = Blueprint('mlservice', __name__, url_prefix='/ml')

def get_columns():
  with open(const.obd_fields) as f:
    lines = f.readlines()
  lines = [line.rstrip('\n') for line in lines]
  return lines

@ml.route('/predict', methods=["GET","POST"])
def predict():
    """
        inputs:
            device_id
            sensor_id
            length (number of data sets to perform prediction on)
            name (name of the clustering model which has a set of columns)
            pretrained (to load pretrained model)

        function:
            gets data from mongodb
            performs clustering prediction on these data points
        return:
            status
            message
            prediction array
            data points array
    """
  
    print("api called")

    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        
        user_id = get_userid(response["email"])
        device_id = request.args.get("device_id")
        sensor_id = request.args.get("sensor_id")
        length = request.args.get("len",9999)
        name = request.args.get("name","")
        cols = request.args.getlist("columns[]") 
        pretrained = request.args.get("pretrained","false")

        g = Clustering(user_id, device_id, sensor_id)
        message = g.predict(name,cols,length,pretrained)
        return jsonify(message)

@ml.route('/clustering', methods=["POST"])
def cluster():
    """
        inputs:
            sensor_id
            device_id
            length (number of records to perform clustering on)
        function:
            gets data from mongodb 
            performs clustering
            saves cluster model as pickle
        return 
            success/exception message with status
    """
    print("api called")

    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
    
        cols = request.form.getlist("columns[]") 
        user_id = get_userid(response["email"])
        device_id = request.form.get("device_id")
        sensor_id = request.form.get("sensor_id")
        name = request.form.get("name")
        n = request.form.get("n")

        period = request.form.get("period","now")
        length = request.form.get("length")

        g = Clustering(user_id, device_id, sensor_id)
        message = g.cluster(name,cols,n,length) 
        return jsonify(message)

@ml.route('/get_models', methods=["GET"])
def get_model():
    """
        inputs:
            device_id
            sensor_id
        function:
            gets clustering model details
        return:
            list of saved models by the user ()
    """
    
    print("api called")

    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
        
        user_id = get_userid(response["email"])
        device_id = request.args.get("device_id")
        sensor_id = request.args.get("sensor_id")

        g = Columns(user_id, device_id, sensor_id)
        message = g.get_models()

        return jsonify(message)

    
@ml.route('/pretrain', methods=["GET"])
def pretrain():
    """
        inputs:
            sensor_id
            device_id
            length (number of records to perform clustering on)
        function:
            gets data from mongodb 
            performs clustering
            saves cluster model as pickle
        return 
            success/exception message with status
    """
    print("pretrain model called")

    #token = request.headers["Authorization"]

    cols  = ["Engine_speed","Vehicle_speed","Absolute_throttle_position","Calculated_LOAD_value"] #overwritten cols for pretrained model 

    user_id = "5b1fa507e8a4df16886bd688"
    device_id = "5acc93a22a92c35117bde7b9"
    sensor_id = "5acc93d82a92c35117bde7c4"
    name = "pre"
    n = 4

    period = "now"
    length = 9900

    g = Clustering(user_id, device_id, sensor_id)
    message = g.cluster(name,cols,n,length,pretrained="true") 
    return jsonify(message)


@ml.route('/save_model', methods=["POST"])
def save_model():
    
    print("api called")

    #token = request.headers["Authorization"]
    response = check_auth()
    if response["status"] == 1:
    
        cols = request.form.getlist("columns[]")
        name = request.form.get("model_name").replace(" ","_").replace("'","_").replace("\"","_")
        n = request.form.get("n")

        user_id = get_userid(response["email"])
        device_id = request.form.get("device_id")
        sensor_id = request.form.get("sensor_id")

        g = Columns(user_id, device_id, sensor_id)
        message = g.put_model(cols,name,n)

        return jsonify(message)


