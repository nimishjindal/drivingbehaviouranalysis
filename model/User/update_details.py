"""
This file is used to update the details of a registered user

"""

import datetime
from bson.objectid import ObjectId
from config.messages  import update_messages
from model.basemodel import Basemodel


class Update(Basemodel):

    def __init__(self, id, inp , field):

        """

        :param id:
        :param status:
        :param pin:
        :param mobile:

        This method initialises the variables/parameters

        """

        Basemodel.__init__(self)
        self.id = id
        self.input = inp
        self.field = field


    def update_details(self):

        """

        This method implements the following functionalities :-
            1)  Tries to update the user details -> If successful -> Return success message
            2)  If it fails -> Return failure message

        """

        print ("In update_details block")

        if self.db.aceuser.update_one({'_id': ObjectId(self.id)}, {'$set': {self.field: self.input,
                                                                           'updatedon': datetime.datetime.utcnow(),
                                                                           }}):
            message = update_messages["MSG_001"]
        else:
            message = update_messages["MSG_002"]

        return message
