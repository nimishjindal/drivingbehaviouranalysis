"""

This file is used for implementing the signout feature of the web application.
The file consists of the User class which has the following methods -
    1) __init__ - Initialises the parameters/variables
    2) signout - Implements the functionality for signout and token updation

"""

import datetime
from config.messages  import signout_messages
from model.basemodel import Basemodel
from bson.objectid import ObjectId


class User(Basemodel):
    def __init__(self):

        """

        This method initialises the variables/parameters

        """

        Basemodel.__init__(self)
        self.user_id = None

    def signout(self, user_id):

        """

        This method updates the token of the user on signing out

        :param user_id:
        :return:

        """
        print ("In signout block")
        self.user_id = user_id
        if self.db.aceuser.update_one({'_id': ObjectId(self.user_id)}, {'$set': {'token': " ", 'updatedon': datetime.datetime.utcnow()}}):
            session.pop('sess_id', None)
            message = signout_messages["MSG_001"]
        else:
            message = signout_messages["MSG_002"]

            return message