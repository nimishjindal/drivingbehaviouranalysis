""""

This file implements the login functionality.

The file consists of the Login class which has the following methods :-
    1) __init__ - Initialises the parameters/variables
    2) login_status -  Validates the user and sends the mail with the OTP
    3) profile_check - Validates the OTP and returns the result

"""


import datetime
import time
#import jwt
from email.mime.text import MIMEText
from random import randint
from bson.objectid import ObjectId
from passlib.hash import pbkdf2_sha256
#from common.mail import send_mail
from config.messages import login_messages
from config import constants as const
from model.basemodel import Basemodel


class Login(Basemodel):

    def __init__(self):

        """
        This method initialises the parameters/variables.

        """
        Basemodel.__init__(self)
        self.email = None
        self.password = None
        self.otp = None

    def doLogin(self, email, password):

        """

        :param email:
        :param password:

        This method implements the following functionalities :-
            1) Initialises the database client
            2) Validates the user using the email ID. If successful, move to step 3, else , return.
            3) Verifies the password entered by the user. If successful, sends a mail with the OTP, else , return.

        """

        print( "In doLogin block")

        self.email = email
        self.password = password
        print(self.db.aceuser.find({}))
        if self.db.aceuser.find({"email": self.email}).count() >= 1:
            item = self.db.aceuser.find_one({"email": self.email})
            db_password = item.get('password')
            id = item.get('_id')
            if pbkdf2_sha256.verify(self.password, db_password):
                """
                otp = randint(1000, 9999)
                self.db.aceuser.update_one({'_id': ObjectId(id)},
                                      {'$set': {'OTP': otp, 'updatedon': datetime.datetime.utcnow()}})
                msg = 'Your OTP for login is : ' + str(otp)
                #print(msg)
                msg = MIMEText(msg, 'html')
                ''' Comment to work with out sending mail '''
                #send_mail(self.email, msg)
                """

                message = login_messages["MSG_001"] #success password matches and email found
            else:
                message = login_messages["MSG_002"] #password doesn't match
        else:
            message = login_messages["MSG_003"] #email not found

        return message

    def validateOTP(self, email):

        """

        :param email:
        :param otp:

        This method implements the following functionalities :-
            1) Validates the OTP entered by the user. If successful, return a JWT and the username, else, return/exit

        """

        print( "In validateOTP block")

        self.email = email
        self.otp = randint(1000, 9999)
        item = self.db.aceuser.find_one({"email": self.email})
        id = item.get('_id')
        #token = jwt.encode({"email" : self.email, "otp" : self.otp}, const.common['SECRET_KEY'], algorithm='HS256')
        token = pbkdf2_sha256.hash(email+str(self.otp))
        

        self.db.aceuser.update_one({'_id': ObjectId(id)},
                                {'$set': {'token': token, 'token_time': time.time(),
                                        'lastLogin': str(datetime.datetime.utcnow())}})
        username = item.get("username")
        status = item.get("status")
        pic = 'assets/images/yashjit.png'
        
        userObj = {"userid": str(id), "username":username, "picture":pic, "email":self.email,"status":status,"token":token}
        message = {"data":userObj, "message": "Success", "token": token, "status" : 200}
        #message = {"message": "Success", "token": token, "status" : 200}

        return message