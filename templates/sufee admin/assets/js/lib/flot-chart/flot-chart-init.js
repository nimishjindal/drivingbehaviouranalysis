
(function($){

 "use strict"; // Start of use strict

 var SufeeAdmin = {

    lineFlot: function(){

		var d1 = [];
		for (var i = 0; i < Math.PI * 2; i += 0.25) {
			d1.push([i, Math.sin(i)]);
		}

		var d2 = [];
		for (var i = 0; i < Math.PI * 2; i += 0.25) {
			d2.push([i, Math.cos(i)]);
		}

		var d3 = [];
		for (var i = 0; i < Math.PI * 2; i += 0.1) {
			d3.push([i, Math.tan(i)]);
		}

		$.plot("#flot-line", [
			{ label: "sin(x)", data: d1 },
			{ label: "cos(x)", data: d2 },
			{ label: "tan(x)", data: d3 }
		], {
			series: {
				points: { show: true }
			},
			xaxis: {
				ticks: [
					0, [ Math.PI/2, "\u03c0/2" ], [ Math.PI, "\u03c0" ],
					[ Math.PI * 3/2, "3\u03c0/2" ], [ Math.PI * 2, "2\u03c0" ]
				]
			},
			yaxis: {
				ticks: 10,
				min: -2,
				max: 2,
				tickDecimals: 3
			},
			grid: {
				borderWidth: {
					top: 1,
					right: 1,
					bottom: 2,
					left: 2
				}
			}
		});

    },

};

$(document).ready(function() {
    SufeeAdmin.lineFlot();
});

})(jQuery);
