"""

This file implements the device listing functionality of the website.
The file consists of the Device class which implements the following methods :-
    1) __init__ - Initialises the variables/parameters
    2) list_devices - Fetches the devices from the database
        a)  If successful -> Returns the success message along with the list of the devices
        b)  If failure -> Returns the failure message

"""


import json
from model.basemodel import Basemodel
from config.messages  import device_search_messages
from bson.objectid import ObjectId
from bson import json_util


class Search(Basemodel):
    def __init__(self):

        """

        This method acts as a constructor

        """

        Basemodel.__init__(self)
        self.device_id = None

    def list_device(self, device_id):

        """

        This method implements the following functionalities :-
            1)  Checks for user in the database
            2) If user exists, fetches the record of the registered devices and returns them

        :param user_id:
        :return:

        """

        self.device_id = device_id

        if self.db.acedevices.find({"_id": ObjectId(self.device_id)}).count() >= 1:  # Device exists in the database
            item = self.db.acedevices.find({"_id" : ObjectId(self.device_id)})
            message = device_search_messages["MSG_001"]
            message["device"] = []
            for record in item:
                d = {}
                d["_id"] = str(record["_id"])
                d["userId"] = str(record["userId"])
                d["device"] = str(record["device"])
                d["devicetype"] = str(record["devicetype"])
                d["devicecode"] = str(record["devicecode"])
                d["macid"] = str(record["macid"])
                d["model"] = str(record["model"])
                d["sshuser"] = str(record["sshuser"])
                d["password"] = str(record["password"])
                d["ipaddress"] = str(record["ipaddress"])
                d["port"] = str(record["port"])
                d["accesspoint"] = str(record["accesspoint"])
                d["networkid"] = str(record["networkid"])
                d["created_on"] = str(record["created_on"])
                d["updatedon"] = str(record["updatedon"])
                d["last_login_on"] = str(record["last_login_on"])
                d["enabled"] = str(record["enabled"])
                d["datapoint"] = str(record["datapoint"])
                d["dataendpoints"] = str(record["dataendpoints"])
                d["longitude"] = str(record["longitude"])
                d["latitude"] = str(record["latitude"])
                d["network_type"] = str(record["network_type"])
                d["sensors"] = []
                d["roles"] = []
                d["rules"] = []
                d["actions"] = []

                for sensor in record["sensors"]:
                    s = {}
                    s["name"] = str(sensor["name"])
                    s["gpio"] = str(sensor["gpio"])
                    s["code"] = str(sensor["code"])
                    s["type"] = str(sensor["type"])
                    s["state"] = str(sensor["state"])
                    s["comm_protocol"] = str(sensor["comm_protocol"])
                    s["timestamp"] = str(sensor["timestamp"])
                    d["sensors"].append(s)

                for role in record["roles"]:
                    ro = {}
                    ro["name"] = str(role["name"])
                    ro["value"] = str(role["value"])
                    ro["code"] = str(role["code"])
                    d["roles"].append(ro)

                for rule in record["rules"]:
                    ru = {}
                    ru["code"] = str(rule["code"])
                    ru["name"] = str(rule["name"])
                    ru["desc"] = str(rule["desc"])
                    ru["value"] = str(rule["value"])
                    ru["param"] = str(rule["param"])
                    ru["condition"] = str(rule["condition"])
                    ru["status"] = str(rule["status"])
                    ru["attrib"] = str(rule["attrib"])
                    d["rules"].append(ru)

                for action in record["actions"]:
                    ac = {}
                    ac["code"] = action["code"]
                    ac["name"] = action["name"]
                    ac["status"] = action["status"]
                    ac["action"] = action["action"]
                    d["actions"].append(ac)

                message["device"].append(d)
        else :
            message = device_search_messages["MSG_002"]

        return message
