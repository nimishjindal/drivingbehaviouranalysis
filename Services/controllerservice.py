from flask import Blueprint, request, jsonify, session,render_template ,redirect, url_for
from flask import current_app as app
import random
import datetime
from werkzeug.exceptions import default_exceptions
import pandas as pd
from config.constants import cols
from common.utils import validate_token, check_auth, get_userid ,check_sess,get_all_columns #, publish

controller = Blueprint('controllerservice', __name__)

@controller.route("/devicedata/<device_id>/<sensor_id>", methods = ["GET", "POST"])
def devicedata(device_id=None,sensor_id=None):
    if(device_id == "none") or (sensor_id == "none"):
        return render_template ("selectdevice.html")
 
    all_obd_cols = get_all_columns()
    all_obd_cols.sort()

    return render_template("sensorData.html",table_cols=all_obd_cols, all_obd_cols = all_obd_cols)

@controller.route("/", methods=["GET"])
def index():
    if check_sess():
        return redirect('/profile')
    return redirect('/login')

@controller.route("/register", methods=["POST","GET"])
def register():

    if check_sess():
        return redirect('/profile')
    return render_template("page-register.html")

@controller.route("/login", methods=["GET"])
def login():
    if check_sess():
        return redirect('/profile')
    return render_template("page-login.html")

@controller.route('/profile', methods=["GET"])
def profile():
    if not check_sess():
        return redirect('/login')

    profile = ["firstName","lastName","email"]

    return render_template("profile.html", fields=profile)

@controller.route('/signout', methods=["GET"],)
def signout():
    session.pop('token', None)
    return redirect("/login")

@controller.route('/testB', methods=['GET', 'POST'])
def testB():

    return render_template("nav.html")

@controller.route("/dashboard/<device_id>/<sensor_id>", methods = ["GET", "POST"])
def dashboard(device_id=None,sensor_id=None):
    if not check_sess():
        return redirect('/login')

    if(device_id == "none") or (sensor_id == "none"):
        return render_template ("selectdevice.html")
     
    return render_template("dashboard.html")

@controller.route("/reset", methods=["GET"])
def forgot():
    if check_sess():
        return redirect('/profile')
    return render_template('pages-forget.html')
