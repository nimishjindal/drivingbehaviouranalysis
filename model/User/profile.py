"""

This file is used for implementing the registration part of the website.
The file consists of the Registration class which has the following methods :-
    1) __init__ - Works as a constructor to initialise the variables/parameters
    2) register_user - Has the code for interacting with the database and implementing the registration section

"""

from config.messages  import profile_messages
from model.basemodel import Basemodel
from bson import ObjectId

class Profile(Basemodel):

    def __init__(self, firstName, lastName, email, website, company, address, country, state, city, zipcode,
                    details, subscribed, createdon, updatedon):

        """
        This method initialises the parameters/variables
        """
        Basemodel.__init__(self)
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.website = website
        self.company = company
        self.address = address
        self.country = country
        self.state = state
        self.city = city
        self.zipcode = zipcode
        self.details = details
        self.subscribed = subscribed
        self.createdon = createdon
        self.updatedon = updatedon

    """"""

    def create_profile(self, user_id):

        """
        This method implements the following functionalitites :-
            1) Initialises the database connection client. If successful, proceed to step 2 else return as exception
            2) Checks if the email id already exists in the database. If it does, return, else, proceed to step 3.
            3) Inserts the form data into the database. If successful, return .

        """

        print ("In Profile block")

        self.userId = user_id
        try:
            if self.db.aceuserprofile.find({"userId": ObjectId(self.userId)}).count() == 0:
                message = profile_messages["MSG_001"]
                return message
            else:
                self.db.aceuserprofile.update({"userId": ObjectId(self.userId)}, {"$set":{
                    "firstName": self.firstName,
                    "lastName": self.lastName,
                    "email": self.email,
                    "website": self.website,
                    "company": self.company,
                    "address": self.address,
                    "country": self.country,
                    "state": self.state,
                    "city": self.city,
                    "zipcode": self.zipcode,
                    "createdon": self.createdon,
                    "updatedon": self.updatedon,
                    "details": self.details,
                    "subscribed": self.subscribed,
                }}, upsert=False)

                message = profile_messages["MSG_002"]
                return message
        except:
            message = profile_messages["MSG_003"]
            return message